package com.motorola.motocaretechnician.unit;

import android.test.AndroidTestCase;
import android.test.mock.MockContext;
import android.util.Patterns;

import com.motorola.motocaretechnician.R;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

public class LoginEmailValidatorTest extends AndroidTestCase {

    private MockContext context;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        context = new MockContext();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testAndroidTestCaseSetupProperly() {
        assertNotNull("Context is null. setContext should be called before tests are run", context);
    }

    public void testEmailValidation() {
        RegexpValidator regexpValidator = new RegexpValidator(context.getString(R.string.login_email_crouton_message_error), Patterns.EMAIL_ADDRESS.pattern());
        assertTrue(regexpValidator.isValid("renannery@gmaul.com", false));
    }
}
