package com.motorola.motocaretechnician.unit;

import android.test.mock.MockContext;

import com.motorola.motocaretechnician.control.DateUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by jsilva on 5/26/15.
 */
public class DateUtilsTest {

    DateUtils dateUtils;

    @Before
    public void setUp() throws Exception {
       dateUtils = new DateUtils();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testDateStringToIso8601() {
        String date = "10/10/2010 10:10";
        String dateIso8602 = DateUtils.getddMMyyyyToIso8601(date);
        assertTrue(dateIso8602.equals("2010-10-10T10:10:00-0300"));
    }

    @Test
    public void testIso8801StringToDate() {
        String dateIso8602 = "2010-10-10T10:10:00-0300";
        String date = DateUtils.getIso8601ddMMyyyy(dateIso8602);
        assertTrue(date.equals("10/10/2010"));
    }

    @Test
    public void testIso8801StringToDateTime() {
        String dateIso8602 = "2010-10-10T10:10:00-0300";
        String dateTime = DateUtils.getIso8601hhmm(dateIso8602);
        assertTrue(dateTime.equals("10:10"));
    }

    @Test
    public void testStringDate() {
        String dateIso8602 = "2010-10-10T10:10:00-0300";
        String dateTime = DateUtils.getDateString(dateIso8602);
        assertTrue(dateTime.equals("Oct 10, 2010"));
    }

    @Test
    public void testConvertDateToDDMMYYYY() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,2010);
        calendar.set(Calendar.MONTH,9);
        calendar.set(Calendar.DAY_OF_MONTH,10);
        String dateTime = DateUtils.convertDateToYYMMDD(calendar.getTime());
        assertTrue(dateTime.equals("10/10/10"));
    }

    @Test
    public void testConvertDateToHHMM() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,5);
        calendar.set(Calendar.MINUTE,30);
        String dateTime = DateUtils.convertDateToHHMM(calendar.getTime());
        assertTrue(dateTime.equals("05:30"));
    }

    @Test
    public void convertStringToDate(){
        String stringDate = "2010-10-10T10:00:00-0300";
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,2010);
        calendar.set(Calendar.MONTH,9);
        calendar.set(Calendar.DAY_OF_MONTH,10);
        calendar.set(Calendar.HOUR_OF_DAY,10);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        Date dateConverted = DateUtils.convertStringToDate(stringDate);
        assertTrue(dateConverted.compareTo(calendar.getTime()) == 0);
    }

}
