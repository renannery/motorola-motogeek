package com.motorola.motocaretechnician.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Dashboard extends RealmObject {

    @PrimaryKey
    private int id;
    private String description;
    private String photo;
    private int count;
    private boolean isChatItem;

    public boolean isChatItem() {
        return isChatItem;
    }

    public void setChatItem(boolean isChatItem) {
        this.isChatItem = isChatItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
