package com.motorola.motocaretechnician.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Device extends RealmObject{


    @PrimaryKey
    private String serialNumber;
    private String manufactureDate;
    private String model;

    private boolean inWarranty;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public boolean isInWarranty() {
        return inWarranty;
    }

    public void setInWarranty(boolean inWarranty) {
        this.inWarranty = inWarranty;
    }
}
