package com.motorola.motocaretechnician.model;


import java.util.ArrayList;

public class GenericBus<T> {

    private int key;
    private Result result = new Result();
    private ArrayList<T> objects;
    private Object object;

    public static final int ACCEPTED_REQUEST_DETAILS = 6;
    public static final int PENDING_REQUEST_DETAILS = 7;
    public static final int REQUES_RECYCLER_VIEW_SCROLL_PENDING = 8;
    public static final int REQUES_RECYCLER_VIEW_SCROLL_ACCEPTED = 9;
    public static final int CHANGE_MAP_TAB = 11;
    public static final int USER_PAST_SERVICES = 21;
    public static final int REQUEST_SCROLL = 22;

    /* USER PROFILE */
    public static final int USER_PROFILE_IMAGE_CROP = 25;
    public static final int USER_LOGIN = 1;
    public static final int USER_FORGOT_PASSWORD = 2;
    public static final int USER_DASHBOARD = 3;
    public static final int USER_NEW_REQUESTS = 4;
    public static final int USER_OPENED_SERVICES = 5;
    public static final int USER_UPLOAD_IMAGE_PROFILE = 30;
    public static final int USER_FILTERED_REQUESTS = 10;

    /* REQUEST LIST */
    public static final int REQUEST_LIST_REFRESH = 23;

    /* SERVICE LIST */
    public static final int SERVICE_LIST_REFRESH = 24;

    /* CHAT */
    public static final int CHAT_MESSAGE_COMPOSING = 12;
    public static final int CHAT_MESSAGE_PAUSED = 13;
    public static final int CHAT_MESSAGE_RECEIVED = 14;
    public static final int CHAT_MESSAGE_ACTIVE = 15;
    public static final int CHAT_MESSAGE_DISPLAYED = 16;
    public static final int CHAT_MESSAGE_BODY = 17;
    public static final int CHAT_MESSAGE_GONE = 18;
    public static final int CHAT_FILE_RECEIVED = 19;

    /* CONNECTION STATE */

    public static final int CONNECTION_STATE = 20;

    /* WORK ORDER DETAILS FLOW */
    public static final int BUTTON_IN_WARRANTY = 25;
    public static final int BUTTON_CREATE_QUOTATION = 26;
    public static final int BUTTON_EDIT_QUOTATION = 26;
    public static final int BUTTON_SEND_QUOTATION = 27;
    public static final int ISSUES_CATEGORIES_LIST = 28;
    public static final int PARTS_CATEGORIES_LIST = 28;
    public static final int REMOVE_PARTS_OR_ISSUES = 29;
    public static final int ADD_ISSUES = 32;
    public static final int UPDATE_WORK_ORDER_ISSUES = 33;


    /* NOTIFICATIONS */
    public static final int NOTIFICATION_UPDATE = 31;


    public GenericBus(int key) {
        this.key = key;
        this.object = null;
    }

    public GenericBus(int key, ArrayList<T> objects, Result result) {
        this.key = key;
        this.objects = objects;
        this.result = result;
    }

    public GenericBus(int key, Object object, Result result) {
        this.key = key;
        this.object = object;
        this.result = result;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public ArrayList<T> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<T> objects) {
        this.objects = objects;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}