package com.motorola.motocaretechnician.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Appointment extends RealmObject {

    @PrimaryKey
    private String id;
    private Scheduling scheduling;
    private String complement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Scheduling getScheduling() {
        return scheduling;
    }

    public void setScheduling(Scheduling scheduling) {
        this.scheduling = scheduling;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
}
