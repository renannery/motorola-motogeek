package com.motorola.motocaretechnician.model;

import io.realm.RealmObject;

public class Scheduling extends RealmObject {
    private Location location;
    private String dateTime;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
