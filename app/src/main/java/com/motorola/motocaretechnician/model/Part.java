package com.motorola.motocaretechnician.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Part extends RealmObject{
    @PrimaryKey
    private long id;
    private String description;
    private String name;
    private String picture;
    private float price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
