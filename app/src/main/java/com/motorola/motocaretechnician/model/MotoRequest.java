package com.motorola.motocaretechnician.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jsilva on 6/8/15.
 */
public class MotoRequest extends RealmObject {

    @PrimaryKey
    private long id;
    private User user;
    private Device device;
    private Appointment appointment;
    private MotoWorkOrder workOrder;
    private RealmList<Issue> issues;
    private String status;


    public RealmList<Issue> getIssues() {
        return issues;
    }

    public void setIssues(RealmList<Issue> issues) {
        this.issues = issues;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public MotoWorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(MotoWorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
