package com.motorola.motocaretechnician.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jsilva on 5/13/15.
 */
public class CategoryIssue extends RealmObject {

    @PrimaryKey
    private long code;
    private String description;
    private RealmList<Issue> issues;


    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<Issue> getIssues() {
        return issues;
    }

    public void setIssues(RealmList<Issue> issues) {
        this.issues = issues;
    }
}
