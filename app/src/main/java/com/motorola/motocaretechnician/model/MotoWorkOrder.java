package com.motorola.motocaretechnician.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MotoWorkOrder extends RealmObject {

    @PrimaryKey
    private String number;
    private String swWarrantyStart;
    private String swWarrantyEnd;
    private String hwWarrantyStart;
    private String hwWarrantyEnd;
    private String typeDescription;
    private String dueDate;
    private String statusDescription;
    private int statusId;
    private RealmList<Part> parts;
    private RealmList<MotoMilestone> milestones;
    private String quotationStatus;
    private boolean isQuotationAccepted;
    private float laborCost;

    public RealmList<Part> getParts() {
        return parts;
    }

    public void setParts(RealmList<Part> parts) {
        this.parts = parts;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSwWarrantyStart() {
        return swWarrantyStart;
    }

    public void setSwWarrantyStart(String swWarrantyStart) {
        this.swWarrantyStart = swWarrantyStart;
    }

    public String getSwWarrantyEnd() {
        return swWarrantyEnd;
    }

    public void setSwWarrantyEnd(String swWarrantyEnd) {
        this.swWarrantyEnd = swWarrantyEnd;
    }

    public String getHwWarrantyStart() {
        return hwWarrantyStart;
    }

    public void setHwWarrantyStart(String hwWarrantyStart) {
        this.hwWarrantyStart = hwWarrantyStart;
    }

    public String getHwWarrantyEnd() {
        return hwWarrantyEnd;
    }

    public void setHwWarrantyEnd(String hwWarrantyEnd) {
        this.hwWarrantyEnd = hwWarrantyEnd;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(String quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public boolean isQuotationAccepted() {
        return isQuotationAccepted;
    }

    public void setIsQuotationAccepted(boolean isQuotationAccepted) {
        this.isQuotationAccepted = isQuotationAccepted;
    }

    public float getLaborCost() {
        return laborCost;
    }

    public void setLaborCost(float laborCost) {
        this.laborCost = laborCost;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }


    public RealmList<MotoMilestone> getMilestones() {
        return milestones;
    }

    public void setMilestones(RealmList<MotoMilestone> milestones) {
        this.milestones = milestones;
    }
}
