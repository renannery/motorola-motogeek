package com.motorola.motocaretechnician.model;

public class ConnectionState {
    private boolean isConnected;
    private int type;

    public ConnectionState(boolean isConnected, int type) {
        this.isConnected = isConnected;
        this.type = type;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
