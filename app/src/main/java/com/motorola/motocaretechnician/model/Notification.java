package com.motorola.motocaretechnician.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Notification extends RealmObject{


    @PrimaryKey
    private String id;
    private int type;
    private String description;
    private String subDescription;
    private String objectGenericId;
    private User user;
    private Date time;

    public String getObjectGenericId() {
        return objectGenericId;
    }

    public void setObjectGenericId(String objectGenericId) {
        this.objectGenericId = objectGenericId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubDescription() {
        return subDescription;
    }

    public void setSubDescription(String subDescription) {
        this.subDescription = subDescription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
