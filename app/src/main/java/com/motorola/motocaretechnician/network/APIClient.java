package com.motorola.motocaretechnician.network;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.motorola.motocaretechnician.control.Config;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public class APIClient {

    private static RestAdapter REST_ADAPTER;

    private static void createAdapterIfNeeded() {
        if (REST_ADAPTER == null) {
            Gson gson = new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(0, TimeUnit.MILLISECONDS);
            REST_ADAPTER = new RestAdapter.Builder()
                    .setEndpoint(Config.PREFIX)
                    .setLogLevel(LogLevel.FULL)
                    .setClient(new OkClient(client))
                    .setConverter(new GsonConverter(gson))
                    .build();
        }
    }

    public APIClient() {
        createAdapterIfNeeded();
    }

    public APIEndpointInterface service() {
        return REST_ADAPTER.create(APIEndpointInterface.class);
    }

    public interface APIEndpointInterface {

        @POST("/users/login") JsonObject returnUser(
                @Body JsonObject userParameters);

        @POST("/users/forgotPwd") JsonObject forgotPassword(
                @Body JsonObject userParameters);

        @GET("/users/{id}/dashboard") JsonObject dashboard(
                @Path("id") String userId);

        @GET("/v1/users/{id}/requests") JsonObject requests(
                @Path("id") String userId);

        @GET("/v1/users/{id}/requests") JsonObject appointments(
                @Path("id") String userId,
                @Query("lat") double lat,
                @Query("lng") double lng,
                @Query("distance") int distance);

        @GET("/v1/users/{id}/openedRequests") JsonObject openServices(
                @Path("id") String userId);

        @GET("/v1/users/{id}/pastRequests") JsonObject pastServices(
                @Path("id") String userId);

        @GET("/v1/issues/list") JsonObject issuesList();
        @GET("/v1/parts/list") JsonObject partsList();

        @GET("/v1/users/{id}/notifications") JsonObject notifications(
                @Path("id") String userId);

        @Multipart
        @POST("/v1/users/{id}/profile_image") JsonObject uploadProfileImage(
                @Path("id") long userId,
                @Part("image") TypedFile image);
    }
}