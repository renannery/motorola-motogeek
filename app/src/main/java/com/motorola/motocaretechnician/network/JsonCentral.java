package com.motorola.motocaretechnician.network;

import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.motorola.motocaretechnician.model.CategoryIssue;
import com.motorola.motocaretechnician.model.CategoryPart;
import com.motorola.motocaretechnician.model.Dashboard;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Appointment;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.MotoWorkOrder;
import com.motorola.motocaretechnician.model.Notification;
import com.motorola.motocaretechnician.model.Result;
import com.motorola.motocaretechnician.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import io.realm.RealmObject;
import retrofit.mime.TypedFile;

public class JsonCentral {

    private static JsonCentral instance;
    private Gson gson;

    private JsonCentral() {
        gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

    }

    public static JsonCentral getInstance() {
        if(instance == null) {
            instance = new JsonCentral();
        }
        return instance;
    }

    public void loginUser (final JsonObject jsonUserParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().returnUser(jsonUserParameters);
                    User user = gson.fromJson(jo.getAsJsonObject("data"), User.class);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_LOGIN, user, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE login user: ".concat(e.toString()));
                    e.printStackTrace();
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_LOGIN, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void dashboard (final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().dashboard(userId);
                    Type typeDashboard = new TypeToken<ArrayList<Dashboard>>() {}.getType();
                    ArrayList<Dashboard> dashboardItems = gson.fromJson(jo.getAsJsonObject("data").getAsJsonArray("dashboardItems"), typeDashboard);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_DASHBOARD, dashboardItems, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE dashboard user: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_DASHBOARD, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void pastServices (final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().pastServices(userId);
                    Type typeWorkOrder = new TypeToken<ArrayList<MotoRequest>>() {}.getType();
                    ArrayList<MotoRequest> motoWorkOrders = gson.fromJson(jo.getAsJsonArray("data"), typeWorkOrder);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_PAST_SERVICES, motoWorkOrders, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE pastServices: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_PAST_SERVICES, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void openedServices (final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().openServices(userId);
                    Type typeWorkOrder = new TypeToken<ArrayList<MotoRequest>>() {}.getType();
                    ArrayList<MotoRequest> motoWorkOrders = gson.fromJson(jo.getAsJsonArray("data"), typeWorkOrder);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_OPENED_SERVICES, motoWorkOrders, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE USER_OPENED_SERVICES user: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_OPENED_SERVICES, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void forgotPassword (final JsonObject jsonUserParameters) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().forgotPassword(jsonUserParameters);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_FORGOT_PASSWORD, null, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE forgot password: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_FORGOT_PASSWORD, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void profileUser(final JsonObject jsonUserParameters) {
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JsonObject jo = new APIClient().service().returnUser(jsonUserParameters);
                    User user = gson.fromJson(jo.getAsJsonObject("data"), User.class);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_LOGIN, user, resultMessage));
                }
            }).start();
        } catch (Exception e) {
            Log.e("ERROR!", "MESSAGE --> PARSE profile: ".concat(e.toString()));
            EventBus.getDefault().post(new GenericBus(GenericBus.USER_LOGIN, null, new Result(false, e.toString())));
        }
    }

    public void uploadProfileImage(final long userId, final TypedFile image) {
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JsonObject jo = new APIClient().service().uploadProfileImage(userId, image);
                    User user = gson.fromJson(jo.getAsJsonObject("data"), User.class);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_UPLOAD_IMAGE_PROFILE, user, resultMessage));
                }
            }).start();
        } catch (Exception e) {
            Log.e("ERROR!", "MESSAGE --> PARSE profile: ".concat(e.toString()));
            EventBus.getDefault().post(new GenericBus(GenericBus.USER_UPLOAD_IMAGE_PROFILE, null, new Result(false, e.toString())));
        }
    }


    public void returnRequests(final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().requests(userId);
                    Type typeRequest = new TypeToken<ArrayList<MotoRequest>>() {}.getType();
                    ArrayList<MotoRequest> motoRequests = gson.fromJson(jo.getAsJsonArray("data"), typeRequest);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_NEW_REQUESTS, motoRequests, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE new requests: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_NEW_REQUESTS, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void returnNotifications(final String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().notifications(userId);
                    Type typeNotification = new TypeToken<ArrayList<Notification>>() {}.getType();
                    ArrayList<Notification> notifications = gson.fromJson(jo.getAsJsonArray("data"), typeNotification);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.NOTIFICATION_UPDATE, notifications, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE notifications: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.NOTIFICATION_UPDATE, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void returnAppointments(final String userId, final double latitude, final double longitude, final int distance) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject jo = new APIClient().service().appointments(userId, latitude, longitude, distance);
                    Type typeAppointment = new TypeToken<ArrayList<Appointment>>() {}.getType();
                    ArrayList<Appointment> appointments = gson.fromJson(jo.getAsJsonArray("data"), typeAppointment);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);

                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_FILTERED_REQUESTS, appointments, resultMessage));
                } catch (Exception e) {
                    Log.e("ERROR!", "MESSAGE --> PARSE new requests: ".concat(e.toString()));
                    EventBus.getDefault().post(new GenericBus(GenericBus.USER_FILTERED_REQUESTS, null, new Result(false, e.toString())));
                }
            }
        }).start();
    }

    public void issuesCategories () {
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JsonObject jo = new APIClient().service().issuesList();
                    Type typeAppointment = new TypeToken<ArrayList<CategoryIssue>>() {}.getType();
                    ArrayList<CategoryIssue> categories = gson.fromJson(jo.getAsJsonArray("data"), typeAppointment);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);
                    EventBus.getDefault().post(new GenericBus(GenericBus.ISSUES_CATEGORIES_LIST, categories, resultMessage));
                }
            }).start();
        } catch (Exception e) {
            Log.e("ERROR!", "MESSAGE --> PARSE profile: ".concat(e.toString()));
            EventBus.getDefault().post(new GenericBus(GenericBus.ISSUES_CATEGORIES_LIST, null, new Result(false, e.toString())));
        }
    }

    public void partsCategories () {
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JsonObject jo = new APIClient().service().partsList();
                    Type typeAppointment = new TypeToken<ArrayList<CategoryPart>>() {}.getType();
                    ArrayList<CategoryPart> categories = gson.fromJson(jo.getAsJsonArray("data"), typeAppointment);
                    Result resultMessage = gson.fromJson(jo.getAsJsonObject("result"), Result.class);
                    EventBus.getDefault().post(new GenericBus(GenericBus.PARTS_CATEGORIES_LIST, categories, resultMessage));
                }
            }).start();
        } catch (Exception e) {
            Log.e("ERROR!", "MESSAGE --> PARSE profile: ".concat(e.toString()));
            EventBus.getDefault().post(new GenericBus(GenericBus.PARTS_CATEGORIES_LIST, null, new Result(false, e.toString())));
        }
    }
}
