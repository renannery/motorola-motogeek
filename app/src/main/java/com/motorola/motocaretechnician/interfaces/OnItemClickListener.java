package com.motorola.motocaretechnician.interfaces;

import android.view.View;

public interface OnItemClickListener {
    public void onClickItem(View view, Object object);
}
