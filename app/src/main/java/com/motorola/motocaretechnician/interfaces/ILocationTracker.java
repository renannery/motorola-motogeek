package com.motorola.motocaretechnician.interfaces;

import android.location.Location;

public interface ILocationTracker {
	public void onConnected();
    public void onLocationChanged(Location location);
	
}
