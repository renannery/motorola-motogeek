package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.ChatMessage;
import com.motorola.motocaretechnician.model.User;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;

public class AdapterChatLobby extends RecyclerView.Adapter<AdapterChatLobby.ViewHolder> {

    private Context context;
    public List<User> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterChatLobby(Context context, List<User> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_chat_lobby, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.fillHolder(dataset.get(i));
        Picasso.with(viewHolder.itemView.getContext())
                .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(i).getPhoto()))
                .centerCrop()
                .fit()
                .into(viewHolder.ivClientPhoto);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(User user) {
        dataset.add(user);
        notifyDataSetChanged();
    }

    public void addItems(Collection<User> users) {
        dataset.addAll(users);
        notifyDataSetChanged();
    }

    public List<User> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvLastMessage)
        TextView tvLastMessage;
        @InjectView(R.id.tvLastMessageDate)
        TextView tvLastMessageDate;
        @InjectView(R.id.ivClientPhoto)
        ImageView ivClientPhoto;

        private OnItemClickListener onItemClickListener;
        private User user;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, user);
        }

        public void fillHolder(User user) {
            this.user = user;
            tvUsername.setText(String.valueOf(user.getFirstName().concat(" ").concat(user.getLastName())));
            Realm realm = Realm.getInstance(itemView.getContext());
            long messagesCount = realm.where(ChatMessage.class)
                    .beginGroup()
                    .equalTo(ChatMessage.MESSAGE_FROM, SessionManager.getInstance().getCurrentUser().getUsername())
                    .equalTo(ChatMessage.MESSAGE_TO, user.getUsername())
                    .endGroup()
                    .or()
                    .beginGroup()
                    .equalTo(ChatMessage.MESSAGE_FROM, user.getUsername())
                    .equalTo(ChatMessage.MESSAGE_TO, SessionManager.getInstance().getCurrentUser().getUsername())
                    .endGroup().count();

            if(messagesCount > 0) {
                ChatMessage lastChatMessage = realm.where(ChatMessage.class)
                        .beginGroup()
                        .equalTo(ChatMessage.MESSAGE_FROM, SessionManager.getInstance().getCurrentUser().getUsername())
                        .equalTo(ChatMessage.MESSAGE_TO, user.getUsername())
                        .endGroup()
                        .or()
                        .beginGroup()
                        .equalTo(ChatMessage.MESSAGE_FROM, user.getUsername())
                        .equalTo(ChatMessage.MESSAGE_TO, SessionManager.getInstance().getCurrentUser().getUsername())
                        .endGroup().findAll().last();
                tvLastMessage.setText(lastChatMessage.getMessage());
                tvLastMessageDate.setText(DateUtils.convertDateToHHMM(lastChatMessage.getDateTime()));
            } else {
                tvLastMessage.setText("");
            }
        }
    }
}
