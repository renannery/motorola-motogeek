package com.motorola.motocaretechnician.control;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by jsilva on 4/8/15.
 */
public class DateUtils {

    private static SimpleDateFormat ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private static SimpleDateFormat HHMM = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat YYMMDD = new SimpleDateFormat("yy/MM/dd");


    public static String getddMMyyyyToIso8601(String dateTime){
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date date = dateFormat.parse(dateTime);
            return ISO8601.format(date);
        }catch(ParseException e){
            e.printStackTrace();
            return "";
        }
    }

    public static String getIso8601ddMMyyyy(String dateTime){
        try{

            Date date = ISO8601.parse(dateTime);
            SimpleDateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
            return ddMMyyyy.format(date);
        }catch(ParseException e){
            e.printStackTrace();
            return "";
        }
    }

    public static String getIso8601hhmm(String dateTime){
        try{

            Date date = ISO8601.parse(dateTime);
            SimpleDateFormat hhmm = new SimpleDateFormat("hh:mm");
            return hhmm.format(date);
        }catch(ParseException e){
            e.printStackTrace();
            return "";
        }
    }


    public static String getDateString(String dateTime){
        try {

            int style = DateFormat.MEDIUM;
            DateFormat localizedDateFormat = DateFormat.getDateInstance(style, Locale.getDefault());
            Date result = ISO8601.parse(dateTime);
            String formattedString = localizedDateFormat.format(result);
            return formattedString;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";

        }
    }


    public static String getTimeString(String dateTime){
        try {

            int style = DateFormat.HOUR_OF_DAY0_FIELD;
            DateFormat localizedDateFormat = DateFormat.getDateInstance(style, Locale.getDefault());
            Date result = ISO8601.parse(dateTime);
            String formattedString = localizedDateFormat.format(result);
            return formattedString;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";

        }
    }

    public static String convertDateToYYMMDD(Date dateTime){
        try {
            return YYMMDD.format(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }

    public static String convertDateToHHMM(Date dateTime){
        try {
            return HHMM.format(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }

    public static String convertISO8601toStringYYMMDD(final String iso8601string) {
        String s = iso8601string.replace("Z", "+00:00");
        try {
            s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
        } catch (IndexOutOfBoundsException e) {
            return "Invalid length";
        }
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertDateToYYMMDD(date);
    }

    public static String convertDateTimeToString(Date dateTime){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return dateFormat.format(dateFormat);
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }

    public static Date currentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new Date();
    }


    public static Date convertStringToDate(String dateString){
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


}
