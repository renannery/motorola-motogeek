package com.motorola.motocaretechnician.control;


import com.androidmapsextensions.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.model.MotoRequest;

public class MarkerFactory {
    private static MarkerFactory instance;

    public static MarkerFactory getInstance() {
        if(instance == null) {
            instance = new MarkerFactory();
        }
        return instance;
    }

    private MarkerFactory() {
    }

    public MarkerOptions getMarker(MotoRequest motoRequest) {
        MarkerOptions marker = new MarkerOptions();
        marker.position(new LatLng(motoRequest.getAppointment().getScheduling().getLocation().getLatitude(), motoRequest.getAppointment().getScheduling().getLocation().getLongitude())).title(String.valueOf(motoRequest.getUser().getFirstName()));

        if(motoRequest.getStatus().equals(Const.APPOINTMENT_ACCEPTED)) {
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pessoanomapa));
        } else if(motoRequest.getStatus().equals(Const.APPOINTMENT_PENDING)) {
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.localvermelho));
        }
        return marker;
    }
}