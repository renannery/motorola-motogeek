package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.model.ChatMessage;

import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterChat extends RecyclerView.Adapter<RecyclerView.ViewHolder > {

    private Context context;
    public List<ChatMessage> dataset;

    public AdapterChat(Context context, List<ChatMessage> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View viewMe = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_chat_me, viewGroup, false);
        View viewThey = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_chat_they, viewGroup, false);

        switch (viewType) {
            case Const.TYPE_CHAT_THEY: return new ViewHolderThey(viewThey);
            case Const.TYPE_CHAT_ME: return new ViewHolderMe(viewMe);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(viewHolder.getItemViewType() == Const.TYPE_CHAT_THEY){
            ViewHolderThey eventHolder = (ViewHolderThey) viewHolder;
            eventHolder.fillHolder(dataset.get(position));
        } else {
            ViewHolderMe chatViewHolder = (ViewHolderMe) viewHolder;
            chatViewHolder.fillHolder(dataset.get(position));
        }
    }


    @Override
    public int getItemCount() {
        return dataset.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (dataset.get(position).getMessageFrom().equals(SessionManager.getInstance().getCurrentUser().getUsername())) {
            return Const.TYPE_CHAT_ME;
        } else {
            return Const.TYPE_CHAT_THEY;
        }
    }

    public void addItem(ChatMessage message) {
        dataset.add(message);
    }

    public void addItems(Collection<ChatMessage> messages) {
        dataset.addAll(messages);
    }

    public static class ViewHolderThey extends RecyclerView.ViewHolder {

        @InjectView(R.id.tvMessage)
        TextView tvMessage;
        @InjectView(R.id.tvMessageTime)
        TextView tvMessageTime;

        public ViewHolderThey(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(ChatMessage chatMessage) {
            tvMessage.setText(chatMessage.getMessage());
            tvMessageTime.setText(DateUtils.convertDateToHHMM(chatMessage.getDateTime()));
        }
    }

    public static class ViewHolderMe extends RecyclerView.ViewHolder {

        @InjectView(R.id.tvMessage)
        TextView tvMessage;
        @InjectView(R.id.tvMessageTime)
        TextView tvMessageTime;

        public ViewHolderMe(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(ChatMessage chatMessage) {
            tvMessage.setText(chatMessage.getMessage());
            tvMessageTime.setText(DateUtils.convertDateToHHMM(chatMessage.getDateTime()));
        }
    }
}
