package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.MotoWorkOrder;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterPastServices extends RecyclerView.Adapter<RecyclerView.ViewHolder > {

    private Context context;
    public List<MotoRequest> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterPastServices(Context context, List<MotoRequest> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_past_services, viewGroup, false);
        return new ViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        setAnimation(viewHolder.itemView, position);
        ViewHolder workOrderHolder = (ViewHolder) viewHolder;
        workOrderHolder.fillHolder(dataset.get(position));
        Picasso.with(viewHolder.itemView.getContext())
                .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(position).getUser().getPhoto()))
                .centerCrop()
                .fit()
                .into(workOrderHolder.ivUser);
    }


    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(MotoRequest motoRequest) {
        dataset.add(motoRequest);
        notifyDataSetChanged();
    }

    public void addItems(Collection<MotoRequest> motoRequest) {
        dataset.addAll(motoRequest);
        notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate, int position) {
        AnimationUtils.slideUpFromBottom(viewToAnimate, context, position * 200);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvWorkOrderNumber)
        TextView tvWorkOrderNumber;
        @InjectView(R.id.tvDevice)
        TextView tvDevice;
        @InjectView(R.id.tvDueDate)
        TextView tvDueDate;
        @InjectView(R.id.tvStatus)
        TextView tvStatus;
        @InjectView(R.id.ivUser)
        ImageView ivUser;

        private OnItemClickListener onItemClickListener;
        private MotoRequest motoRequest;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, motoRequest);
        }

        public void fillHolder(MotoRequest motoRequest) {
            this.motoRequest = motoRequest;

            tvUsername.setText(motoRequest.getUser().getFirstName().concat(" ").concat(motoRequest.getUser().getLastName()));
            if(motoRequest.getWorkOrder() != null){
                tvWorkOrderNumber.setText(motoRequest.getWorkOrder().getNumber());
                tvStatus.setText(motoRequest.getWorkOrder().getStatusDescription());
                tvDueDate.setText(DateUtils.convertISO8601toStringYYMMDD(motoRequest.getWorkOrder().getDueDate().toString()));
            }
            tvDevice.setText(motoRequest.getDevice().getModel());
        }
    }
}
