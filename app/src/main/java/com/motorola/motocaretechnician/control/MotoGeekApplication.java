package com.motorola.motocaretechnician.control;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.motorola.motocaretechnician.BuildConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by rnery on 5/29/15.
 */
public class MotoGeekApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.USE_CRASHLYTICS) {
            Fabric.with(this, new Crashlytics());
        }
    }
}
