package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.ILocationTracker;

public class PositionManager implements  GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    private static PositionManager instance;
    private LocationRequest locationRequest;
    private LocationManager locationManager;
    private ILocationTracker locationTracker;
    private Context context;
    private GoogleApiClient googleApiClient;

    private PositionManager() {

    }

    public static PositionManager getInstance() {
        if (instance == null) {
            instance = new PositionManager();
        }

        return instance;
    }

    public void initialize(ILocationTracker locationTracker, Context context) {
        this.locationTracker = locationTracker;
        this.context = context;
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    public void finalize(ILocationTracker locationTracker) {
        if (this.locationTracker == locationTracker) {
            googleApiClient.disconnect();
        }
    }

    public boolean isGpsEnabled (Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public Location getLocation() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(context, context.getString(R.string.toast_gps_connection_failed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        locationTracker.onLocationChanged(location);
        locationTracker.onConnected();
    }
}