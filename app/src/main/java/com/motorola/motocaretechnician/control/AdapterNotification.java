package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.Notification;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterNotification extends RecyclerView.Adapter<RecyclerView.ViewHolder > {

    private List<Notification> dataset;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public AdapterNotification(Context context, List<Notification> dataset) {
        this.dataset = dataset;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View viewChat = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_notification_chat, viewGroup, false);
        View viewWorkOrder = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_notification_work_order, viewGroup, false);
        View viewRequest = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_notification_request, viewGroup, false);

        switch (viewType) {
            case Const.TYPE_NOTIFICATION_CHAT:
                return new ViewHolderChat(viewChat, onItemClickListener);
            case Const.TYPE_NOTIFICATION_WORK_ORDER:
                return new ViewHolderWorkOrder(viewWorkOrder, onItemClickListener);
            case Const.TYPE_NOTIFICATION_REQUEST:
                return new ViewHolderRequest(viewRequest, onItemClickListener);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(viewHolder.getItemViewType() == Const.TYPE_NOTIFICATION_CHAT){
            ViewHolderChat chatHolder = (ViewHolderChat) viewHolder;
            chatHolder.fillHolder(dataset.get(position));
            Picasso.with(viewHolder.itemView.getContext())
                    .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(position).getUser().getPhoto()))
                    .centerCrop()
                    .fit()
                    .into(chatHolder.ivClientPhoto);
        }

        if(viewHolder.getItemViewType() == Const.TYPE_NOTIFICATION_WORK_ORDER){
            ViewHolderWorkOrder workOrderHolder = (ViewHolderWorkOrder) viewHolder;
            workOrderHolder.fillHolder(dataset.get(position));
            Picasso.with(viewHolder.itemView.getContext())
                    .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(position).getUser().getPhoto()))
                    .centerCrop()
                    .fit()
                    .into(workOrderHolder.ivClientPhoto);
        }

        if(viewHolder.getItemViewType() == Const.TYPE_NOTIFICATION_REQUEST){
            ViewHolderRequest requestHolder = (ViewHolderRequest) viewHolder;
            requestHolder.fillHolder(dataset.get(position));
            Picasso.with(viewHolder.itemView.getContext())
                    .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(position).getUser().getPhoto()))
                    .centerCrop()
                    .fit()
                    .into(requestHolder.ivClientPhoto);
        }
    }


    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (dataset.get(position).getType()) {
            case Const.TYPE_NOTIFICATION_CHAT:
                return Const.TYPE_NOTIFICATION_CHAT;
            case Const.TYPE_NOTIFICATION_WORK_ORDER:
                return Const.TYPE_NOTIFICATION_WORK_ORDER;
            case Const.TYPE_NOTIFICATION_REQUEST:
                return Const.TYPE_NOTIFICATION_REQUEST;
            default:
                return 0;
        }
    }

    public void addItem(Notification message) {
        dataset.add(message);
    }

    public void addItems(Collection<Notification> messages) {
        dataset.addAll(messages);
    }

    public static class ViewHolderWorkOrder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvWorkOrderNumber)
        TextView tvWorkOrderNumber;
        @InjectView(R.id.tvWorkOrderStatus)
        TextView tvWorkOrderStatus;
        @InjectView(R.id.tvDateTime)
        TextView tvDateTime;
        @InjectView(R.id.ivClientPhoto)
        ImageView ivClientPhoto;

        private OnItemClickListener onItemClickListener;
        private Notification notification;

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, notification);
        }

        public ViewHolderWorkOrder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(Notification notification) {
            this.notification = notification;
            tvUsername.setText(String.valueOf(notification.getUser().getFirstName().concat(" ").concat(notification.getUser().getLastName())));
            tvWorkOrderNumber.setText(notification.getDescription());
            tvWorkOrderStatus.setText(notification.getSubDescription());
            tvDateTime.setText(DateUtils.convertDateToHHMM(notification.getTime()));
        }
    }

    public static class ViewHolderRequest extends RecyclerView.ViewHolder implements View.OnClickListener{

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvIssue)
        TextView tvIssue;
        @InjectView(R.id.tvAddress)
        TextView tvAddress;
        @InjectView(R.id.tvDateTime)
        TextView tvDateTime;
        @InjectView(R.id.ivClientPhoto)
        ImageView ivClientPhoto;

        private OnItemClickListener onItemClickListener;
        private Notification notification;

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, notification);
        }

        public ViewHolderRequest(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(Notification notification) {
            this.notification = notification;
            tvUsername.setText(String.valueOf(notification.getUser().getFirstName().concat(" ").concat(notification.getUser().getLastName())));
            tvIssue.setText(notification.getDescription());
            tvAddress.setText(notification.getSubDescription());
            tvDateTime.setText(DateUtils.convertDateToHHMM(notification.getTime()));
        }
    }

    public static class ViewHolderChat extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvLastMessage)
        TextView tvLastMessage;
        @InjectView(R.id.tvDateTime)
        TextView tvDateTime;
        @InjectView(R.id.ivClientPhoto)
        ImageView ivClientPhoto;

        private OnItemClickListener onItemClickListener;
        private Notification notification;

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, notification);
        }

        public ViewHolderChat(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(Notification notification) {
            this.notification = notification;
            tvUsername.setText(String.valueOf(notification.getUser().getFirstName().concat(" ").concat(notification.getUser().getLastName())));
            tvLastMessage.setText(notification.getDescription());
            tvDateTime.setText(DateUtils.convertDateToHHMM(notification.getTime()));
        }
    }
}
