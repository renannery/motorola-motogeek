package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.MotoMilestone;
import com.motorola.motocaretechnician.model.Part;
import com.motorola.motocaretechnician.view.ActivityWorkOrder;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jsilva on 6/15/15.
 */
public class AdapterMotoMilestones extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public List<MotoMilestone> dataset;

    public AdapterMotoMilestones(Context context, List<MotoMilestone> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_work_order_milestone, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public List<MotoMilestone> getItems() {
        return dataset;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder milestoneHolder = (ViewHolder) viewHolder;
        milestoneHolder.fillHolder(dataset.get(i));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        @InjectView(R.id.tvDate)
        TextView tvDate;

        @InjectView(R.id.tvDescription)
        TextView tvDescription;

        private MotoMilestone milestone;

        private View view;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
            this.view = view;
        }


        public void fillHolder(final MotoMilestone milestone) {
            this.milestone = milestone;
            tvDate.setText(DateUtils.convertISO8601toStringYYMMDD(milestone.getDueDate()));
            tvDescription.setText(milestone.getDescription());
        }
    }
}
