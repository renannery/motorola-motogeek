package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.motorola.motocaretechnician.model.User;
import com.motorola.motocaretechnician.view.ActivityChat;
import com.motorola.motocaretechnician.view.ActivityCropImage;
import com.motorola.motocaretechnician.view.ActivityForgotPassword;
import com.motorola.motocaretechnician.view.ActivityJoinUs;
import com.motorola.motocaretechnician.view.ActivityMain;
import com.motorola.motocaretechnician.view.ActivityProfile;
import com.motorola.motocaretechnician.view.ActivityRequestDetails;
import com.motorola.motocaretechnician.view.ActivityWelcome;
import com.motorola.motocaretechnician.view.ActivityWorkOrder;
import com.motorola.motocaretechnician.view.FragmentAcceptedRequestAppointment;
import com.motorola.motocaretechnician.view.FragmentAcceptedRequestDetails;
import com.motorola.motocaretechnician.view.FragmentAppointmentEdit;
import com.motorola.motocaretechnician.view.FragmentChatListLobby;
import com.motorola.motocaretechnician.view.FragmentCreateWorkOrder;
import com.motorola.motocaretechnician.view.FragmentCurrentServices;
import com.motorola.motocaretechnician.view.FragmentDashboard;
import com.motorola.motocaretechnician.view.FragmentDisclaimer;
import com.motorola.motocaretechnician.view.FragmentIssuesCategoriesList;
import com.motorola.motocaretechnician.view.FragmentIssuesList;
import com.motorola.motocaretechnician.view.FragmentLogin;
import com.motorola.motocaretechnician.view.FragmentPartDetail;
import com.motorola.motocaretechnician.view.FragmentPartsCategoriesList;
import com.motorola.motocaretechnician.view.FragmentPartsList;
import com.motorola.motocaretechnician.view.FragmentPastServices;
import com.motorola.motocaretechnician.view.FragmentPendingRequestDetails;
import com.motorola.motocaretechnician.view.FragmentQuotationEdit;
import com.motorola.motocaretechnician.view.FragmentRequestsTabs;
import com.motorola.motocaretechnician.view.FragmentWelcome;
import com.motorola.motocaretechnician.view.FragmentWorkOrderAppointment;
import com.motorola.motocaretechnician.view.FragmentWorkOrderDetails;
import com.motorola.motocaretechnician.view.FragmentWorkOrderIssuesCategoriesList;
import com.motorola.motocaretechnician.view.FragmentWorkOrderIssuesList;

import java.util.Locale;

public class CallManager {

    public static final Intent main(Context context) {
        User user = UserUtils.getInstance().getLoggedUser(context);
        SessionManager.getInstance().setCurrentUser(user);
        return new Intent(context, ActivityMain.class);
    }

    public static final Intent profile(Context context) {
        return new Intent(context, ActivityProfile.class);
    }

    public static final Intent welcome(Context context) {
        return new Intent(context, ActivityWelcome.class);
    }

    public static final Intent requestDetails(Context context) {
        return new Intent(context, ActivityRequestDetails.class);
    }

    public static final Intent imageCropper(Context context) {
        return new Intent(context, ActivityCropImage.class);
    }

    public static final Intent joinUs(Context context) {
        return new Intent(context, ActivityJoinUs.class);
    }

    public static final Intent goToWebsite(String url) {
        return new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
    }

    public static final Intent forgotPassword(Context context) {
        return new Intent(context, ActivityForgotPassword.class);
    }

    public static final Intent startChat(Context context) {
        return new Intent(context, ActivityChat.class);
    }

    public static final Intent workOrderDetail(Context context) {
        return new Intent(context, ActivityWorkOrder.class);
    }

    public static final Intent dialNumberToCall(String phone) {
        Uri call = Uri.parse("tel:" + phone);
        return new Intent(Intent.ACTION_DIAL, call);
    }

    public static final Intent map(double latitude, double longitude) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        return new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    }

    public static final Fragment fragmentDashboard() {
        Fragment fragment = new FragmentDashboard();
        return fragment;
    }

    public static final Fragment fragmentRequestsMap() {
        Fragment fragment = new FragmentRequestsTabs();
        return fragment;
    }

    public static final Fragment fragmentOpenedServices() {
        Fragment fragment = new FragmentCurrentServices();
        return fragment;
    }

    public static final Fragment fragmentPastServices() {
        Fragment fragment = new FragmentPastServices();
        return fragment;
    }

    public static final Fragment fragmentWelcome() {
        Fragment fragment = new FragmentWelcome();
        return fragment;
    }

    public static final Fragment fragmentChatLobby() {
        Fragment fragment = new FragmentChatListLobby();
        return fragment;
    }

    public static final Fragment fragmentDisclaimer() {
        Fragment fragment = new FragmentDisclaimer();
        return fragment;
    }

    public static final Fragment fragmentAcceptRequestDetails() {
        Fragment fragment = new FragmentAcceptedRequestDetails();
        return fragment;
    }

    public static final Fragment fragmentPendingRequestDetails() {
        Fragment fragment = new FragmentPendingRequestDetails();
        return fragment;
    }

    public static final Fragment fragmentLogin() {
        Fragment fragment = new FragmentLogin();
        return fragment;
    }

    public static final Fragment fragmentAppointmentEdit() {
        Fragment fragment = new FragmentAppointmentEdit();
        return fragment;
    }

    public static final Fragment fragmentCreateWorkOrder() {
        Fragment fragment = new FragmentCreateWorkOrder();
        return fragment;
    }

    public static final Fragment fragmentWorkOrderDetails() {
        Fragment fragment = new FragmentWorkOrderDetails();
        return fragment;
    }

    public static final Fragment fragmentQuotationEdit() {
        Fragment fragment = new FragmentQuotationEdit();
        return fragment;
    }

    public static final Fragment fragmentIssuesCategoriesList() {
        Fragment fragment = new FragmentIssuesCategoriesList();
        return fragment;
    }

    public static final Fragment fragmentWorkOrderIssuesCategoriesList() {
        Fragment fragment = new FragmentWorkOrderIssuesCategoriesList();
        return fragment;
    }

    public static final Fragment fragmentPartsCategoriesList() {
        Fragment fragment = new FragmentPartsCategoriesList();
        return fragment;
    }

    public static final Fragment fragmentWorkOrderIssuesList() {
        Fragment fragment = new FragmentWorkOrderIssuesList();
        return fragment;
    }

    public static final Fragment fragmentIssuesList() {
        Fragment fragment = new FragmentIssuesList();
        return fragment;
    }

    public static final Fragment fragmentPartsList() {
        Fragment fragment = new FragmentPartsList();
        return fragment;
    }

    public static final Fragment fragmentPartDetail() {
        Fragment fragment = new FragmentPartDetail();
        return fragment;
    }

    public static final Fragment fragmentWorkOrderAppointmentEdit() {
        Fragment fragment = new FragmentWorkOrderAppointment();
        return fragment;
    }

    public static final Fragment fragmentAcceptedRequestAppointmentEdit() {
        Fragment fragment = new FragmentAcceptedRequestAppointment();
        return fragment;
    }
}
