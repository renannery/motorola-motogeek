package com.motorola.motocaretechnician.control;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Issue;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class AdapterMotoWorkOrderIssuesSelected extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static ArrayList<Issue> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterMotoWorkOrderIssuesSelected(ArrayList<Issue> dataset) {
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_moto_issue_part_selected, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

        ViewHolder appointmentHolder = (ViewHolder) viewHolder;
        appointmentHolder.fillHolder(dataset.get(i));

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(Issue partIssue) {
        dataset.add(partIssue);
        notifyDataSetChanged();
    }

    public void addItems(Collection<Issue> appointments) {
        dataset.addAll(appointments);
        notifyDataSetChanged();
    }

    public ArrayList<Issue> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvIssue)
        TextView tvIssue;

        private OnItemClickListener onItemClickListener;
        private Issue partIssue;
        private View view;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            this.view = view;
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.llDelete)
        public void onClickDelete(){
            dataset.remove(getPosition());
            EventBus.getDefault().post(new GenericBus(GenericBus.UPDATE_WORK_ORDER_ISSUES, null, null));
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, partIssue);
        }

        public void fillHolder(Issue partIssue) {
            this.partIssue = partIssue;
            tvIssue.setText(partIssue.getDescription());
        }
    }
}
