package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.Appointment;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterMotoAcceptedRequests extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public List<MotoRequest> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterMotoAcceptedRequests(Context context, List<MotoRequest> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_moto_accepted_requests, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder appointmentHolder = (ViewHolder) viewHolder;
        appointmentHolder.fillHolder(dataset.get(i));

        Picasso.with(viewHolder.itemView.getContext())
                .load(ServerCentral.getInstance().getImageWithPath(context, dataset.get(i).getUser().getPhoto()))
                .centerCrop()
                .fit()
                .into(appointmentHolder.ivClientPhoto);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(MotoRequest motoRequest) {
        dataset.add(motoRequest);
        notifyDataSetChanged();
    }

    public void addItems(Collection<MotoRequest> motoRequests) {
        dataset.addAll(motoRequests);
        notifyDataSetChanged();
    }

    public List<MotoRequest> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.tvUsername)
        TextView tvUsername;
        @InjectView(R.id.tvAddress)
        TextView tvAddress;
        @InjectView(R.id.tvIssue)
        TextView tvIssue;
        @InjectView(R.id.tvDistance)
        TextView tvDistance;
        @InjectView(R.id.ivClientPhoto)
        ImageView ivClientPhoto;

        private OnItemClickListener onItemClickListener;
        private MotoRequest motoRequest;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, motoRequest);
        }

        public void fillHolder(MotoRequest motoRequest) {
            this.motoRequest = motoRequest;

            tvUsername.setText(String.valueOf(motoRequest.getUser().getFirstName().concat(" ").concat(motoRequest.getUser().getLastName())));
            tvAddress.setText(motoRequest.getAppointment().getScheduling().getLocation().getStreet());
            tvIssue.setText(motoRequest.getIssues().get(0).getDescription());
            float[] results = new float[1];
            Location.distanceBetween(
                    motoRequest.getAppointment().getScheduling().getLocation().getLatitude(), motoRequest.getAppointment().getScheduling().getLocation().getLongitude(),
                    -23.5564, -46.7037, results);


            tvDistance.setText(new DecimalFormat("#.#").format(results[0]/1000));
        }
    }
}
