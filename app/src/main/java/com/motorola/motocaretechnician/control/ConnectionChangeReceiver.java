package com.motorola.motocaretechnician.control;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.motorola.motocaretechnician.model.ConnectionState;
import com.motorola.motocaretechnician.model.GenericBus;

import de.greenrobot.event.EventBus;

public class ConnectionChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent ){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();

        boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();

        if(isConnected) {
            boolean isWiFi = activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI;
            boolean isMobile = activeNetInfo.getType() == ConnectivityManager.TYPE_MOBILE;

            if(isWiFi) {
                EventBus.getDefault().post(new GenericBus(GenericBus.CONNECTION_STATE, new ConnectionState(isConnected, ConnectivityManager.TYPE_WIFI), null));
                return;
            }

            if(isMobile) {
                EventBus.getDefault().post(new GenericBus(GenericBus.CONNECTION_STATE, new ConnectionState(isConnected, ConnectivityManager.TYPE_MOBILE), null));
                return;
            }
        } else {
            EventBus.getDefault().post(new GenericBus(GenericBus.CONNECTION_STATE, new ConnectionState(isConnected, -1), null));
        }
    }
}