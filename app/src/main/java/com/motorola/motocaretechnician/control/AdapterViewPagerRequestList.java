package com.motorola.motocaretechnician.control;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.motorola.motocaretechnician.view.FragmentAcceptedRequestsList;
import com.motorola.motocaretechnician.view.FragmentDisclaimer;
import com.motorola.motocaretechnician.view.FragmentPendingRequestsList;
import com.motorola.motocaretechnician.view.FragmentWelcome;

public class AdapterViewPagerRequestList extends FragmentStatePagerAdapter {
    CharSequence titles[];
    int numbOfTabs;

    public AdapterViewPagerRequestList(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.titles = mTitles;
        this.numbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0) {
            return new FragmentPendingRequestsList();
        } else {
            return new FragmentAcceptedRequestsList();
        }


    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return numbOfTabs;
    }
}