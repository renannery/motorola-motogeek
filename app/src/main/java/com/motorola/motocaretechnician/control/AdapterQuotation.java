package com.motorola.motocaretechnician.control;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.model.Part;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterQuotation extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public ArrayList<Part> dataset;

    public AdapterQuotation(ArrayList<Part> dataset) {
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_quotation, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder appointmentHolder = (ViewHolder) viewHolder;
        appointmentHolder.fillHolder(dataset.get(i));

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addItem(Part partIssue) {
        dataset.add(partIssue);
        notifyDataSetChanged();
    }

    public void addItems(Collection<Part> appointments) {
        dataset.addAll(appointments);
        notifyDataSetChanged();
    }

    public List<Part> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.tvPartName)
        TextView tvPartName;

        @InjectView(R.id.tvPrice)
        TextView tvPrice;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void fillHolder(final Part part) {
            tvPartName.setText(part.getDescription());
            tvPrice.setText(String.format("%.2f", part.getPrice()));
        }
    }
}
