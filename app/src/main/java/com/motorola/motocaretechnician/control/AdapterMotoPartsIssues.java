package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryIssue;

import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterMotoPartsIssues extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public List<CategoryIssue> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterMotoPartsIssues(Context context, List<CategoryIssue> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_moto_parts_issues, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder appointmentHolder = (ViewHolder) viewHolder;
        appointmentHolder.fillHolder(dataset.get(i));

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(CategoryIssue partIssue) {
        dataset.add(partIssue);
        notifyDataSetChanged();
    }

    public void addItems(Collection<CategoryIssue> categoriesIssues) {
        dataset.addAll(categoriesIssues);
        notifyDataSetChanged();
    }

    public List<CategoryIssue> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @InjectView(R.id.tvPartIssue)
        TextView tvPartIssue;

        private OnItemClickListener onItemClickListener;
        private CategoryIssue partIssue;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, partIssue);
        }

        public void fillHolder(CategoryIssue partIssue) {
            this.partIssue = partIssue;


            tvPartIssue.setText(partIssue.getDescription());
        }
    }
}
