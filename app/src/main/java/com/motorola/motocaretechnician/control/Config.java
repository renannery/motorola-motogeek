package com.motorola.motocaretechnician.control;

public class Config {

    /* API */
    public static final String PREFIX = "http://private-4c7d6-motorola.apiary-mock.com";
    public static final String XMPP_HOST = "jabber.rootbash.com";
    public static final String API_VERSION = "/v1";

}