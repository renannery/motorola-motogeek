package com.motorola.motocaretechnician.control;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.motorola.motocaretechnician.model.Issue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class Utils {
	public static Utils instance;
    public ArrayList<Issue> selectedIssues;


    public static Utils getInstance() {
		if(instance == null) {
			instance = new Utils();
		}
		return instance;
	}

    public DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }

    public ProgressDialog createProgressDialog(Context context, String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
        progressDialog.show();
        return progressDialog;
    }

    public void finalizeProgressDialog(ProgressDialog progressDialog) {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public boolean isPlayServicesConfigured(Context context) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if(status == ConnectionResult.SUCCESS)
            return true;
        else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity) context, status);
            dialog.show();
            return false;
        }
    }

    public String issuesArrayToString(RealmList<Issue> issues) {
        ArrayList<String> issuesString = new ArrayList<String>();
        for (Issue issue: issues) {
            issuesString.add(issue.getDescription());
        }
        return TextUtils.join("\n", issuesString);
    }

    public String convertBitmapToFile(Context context, String filename, Bitmap bitmap) {

        try {
            File f = new File(context.getCacheDir(), filename);
            f.createNewFile();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f.getPath();
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    public int resizeRecyclerViewHeight(int adapterHeight, int itemsCount){
        return adapterHeight * itemsCount;
    }

    public void openImageIntent(Context context, String filename, int requestCode) {

        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        root.mkdirs();
        final File sdImageMainDirectory = new File(root, filename);
        Uri outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        ((Activity) context).startActivityForResult(chooserIntent, requestCode);
    }

    public ArrayList<Issue> getWorkOrderSelectedIssues() {
        return selectedIssues;
    }

    public void setWorkOrderSelectedIssues(ArrayList<Issue> issues) {
        selectedIssues = issues;
    }
}