package com.motorola.motocaretechnician.control;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.Part;
import com.motorola.motocaretechnician.view.ActivityWorkOrder;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AdapterMotoParts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public List<Part> dataset;
    private OnItemClickListener onItemClickListener;

    public AdapterMotoParts(Context context, List<Part> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_moto_parts, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder appointmentHolder = (ViewHolder) viewHolder;
        appointmentHolder.fillHolder(dataset.get(i));

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void addItem(Part partIssue) {
        dataset.add(partIssue);
        notifyDataSetChanged();
    }

    public void addItems(Collection<Part> appointments) {
        dataset.addAll(appointments);
        notifyDataSetChanged();
    }

    public List<Part> getItems() {
        return dataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @InjectView(R.id.tvPartName)
        TextView tvPartName;

        @InjectView(R.id.tvPartDescription)
        TextView tvPartDescription;

        @InjectView(R.id.ivPartPicture)
        ImageView ivPartPicture;

        private OnItemClickListener onItemClickListener;
        private Part part;

        private View view;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.onItemClickListener = listener;
            view.setOnClickListener(this);
            ButterKnife.inject(this, view);
            this.view = view;
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClickItem(view, part);
        }

        public void fillHolder(final Part part) {
            this.part = part;
            tvPartName.setText(part.getDescription());
            tvPartDescription.setText(part.getName());


            Picasso.with(view.getContext())
                    .load(part.getPicture())
                    .centerCrop()
                    .fit()
                    .into(ivPartPicture);


            ivPartPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putLong(Const.PART_ID, part.getId());
                    Fragment fragment = CallManager.fragmentPartDetail();
                    fragment.setArguments(bundle);
                    ((ActivityWorkOrder)view.getContext()).setFragmentIssues(fragment, true);
                }
            });

        }
    }
}
