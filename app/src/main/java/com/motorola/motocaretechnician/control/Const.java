package com.motorola.motocaretechnician.control;

public class Const {

    /* DEVICES */
    public static final String DEVICE_COLUMN_MODEL = "model";

    /* DASHBOARD */
    public static final int TYPE_REGULAR_DASHBOARD_ITEM = 0;
    public static final int TYPE_CHAT_DASHBOARD_ITEM = 1;

    /* OPENED SERVICES */
    public static final int TYPE_OPENED_SERVICES = 2;

    /* MOTO REQUEST */
    public static final int TYPE_PENDING_REQUEST_LIST = 3;
    public static final int TYPE_ACCEPTED_REQUEST_LIST = 4;
    public static final String REQUEST_DETAIL_FRAGMENT_TO_INFLATE = "request_details";
    public static final String MOTO_REQUEST_ID = "id";

    /* PARTS */
    public static final String PART_ID = "id";

    /* ISSUES */
    public static final String ISSUE_ID = "id";

    /* APPOINTMENT */
    public static final String APPOINTMENT_STATUS_FIELD = "status";
    public static final String APPOINTMENT_CODE_FIELD = "id";
    public static final String APPOINTMENT_ACCEPTED = "accepted";
    public static final String APPOINTMENT_PENDING = "pending";

    /* WORK ORDER */
    public static final String WORK_ORDER_NUMBER = "number";
    public static final String WORK_ORDER_ISSUE = "work_order_issue";

    /* WORK ORDER STATUS */
    public static final int WAITING_ANALYSIS = 1;
    public static final int WAITING_APPROVAL = 2;
    public static final int QUOTE_REJECTED = 3;
    public static final int WAITING_PARTS = 4;
    public static final int WAITING_REPAIR = 5;
    public static final int REPAIRED = 6;
    public static final int DONE = 7;

    /* USER */
    public static final String REALM_USER_FIRST_NAME = "firstName";


    /* CHAT */
    public static final int TYPE_CHAT_ME = 5;
    public static final int TYPE_CHAT_THEY = 6;

    /* NOTIFICATION */
    public static final int TYPE_NOTIFICATION_CHAT = 1;
    public static final int TYPE_NOTIFICATION_WORK_ORDER = 2;
    public static final int TYPE_NOTIFICATION_REQUEST = 3;

    /* USER */
    public static final String USER_USERNAME = "username";

}