package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;

public class ActivityRequestDetails extends BaseActivity {

    @Override
    protected void doOnFirstTime() {
        Bundle b = getIntent().getExtras();
        int value = b.getInt(Const.REQUEST_DETAIL_FRAGMENT_TO_INFLATE);
        Fragment fragment;
        switch (value) {
            case Const.TYPE_ACCEPTED_REQUEST_LIST:
                fragment = CallManager.fragmentAcceptRequestDetails();
                fragment.setArguments(b);
                setFragmentRequestDetails(fragment, false);
                break;
            case Const.TYPE_PENDING_REQUEST_LIST:
                fragment = CallManager.fragmentPendingRequestDetails();
                fragment.setArguments(b);
                setFragmentRequestDetails(fragment, false);
                break;
        }
    }

    @Override
    protected void doOnCreated(Bundle savedInstanceState) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_request_details;
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }
}
