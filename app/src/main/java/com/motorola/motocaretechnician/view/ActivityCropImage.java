package com.motorola.motocaretechnician.view;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.edmodo.cropper.CropImageView;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.model.GenericBus;

import java.io.File;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class ActivityCropImage extends BaseActivity {

    private static final int ACTIVITY_CROP_IMAGE_GALLERY = 0xabc;
    private static final int ACTIVITY_CROP_IMAGE_CAMERA = 0xcba;
    private static final String ACTIVITY_CROP_IMAGE_FILENAME = "tech_profile.png";
    Intent pictureActionIntent = null;

    @InjectView(R.id.cropImageView)
    CropImageView cropImageView;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.buttonOk)
    Button buttonOk;

    @InjectView(R.id.buttonCancel)
    Button buttonCancel;

    @OnClick(R.id.buttonOk)
    public void onButtonOkClick() {
        String imagePath = Utils.getInstance().convertBitmapToFile(this, ACTIVITY_CROP_IMAGE_FILENAME, cropImageView.getCroppedImage());

        EventBus.getDefault().post(new GenericBus(GenericBus.USER_PROFILE_IMAGE_CROP, imagePath, null));
        finish();
    }

    @OnClick(R.id.buttonCancel)
    public void onButtonCancelClick() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(getString(R.string.activity_profile_picture_crop_toolbar));

        cropImageView.setAspectRatio(5, 10);
        cropImageView.setGuidelines(1);
    }

    @Override
    public int layoutToInflate() {
        return R.layout.activity_image_crop;
    }

    @Override
    protected void doOnFirstTime() {}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case ACTIVITY_CROP_IMAGE_GALLERY:
                if(resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, false);
                        cropImageView.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    finish();
                }
            case ACTIVITY_CROP_IMAGE_CAMERA:
                if(resultCode == RESULT_OK) {
                    try {
                        Bitmap bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                        cropImageView.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    finish();
                }
        }
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, ACTIVITY_CROP_IMAGE_GALLERY);
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }

    public void capturePhoto(String fileName) {
        try {
            File f = new File(fileName);

            if(f.exists() && f.canWrite()) {
                f.delete();
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,Uri.fromFile(f));
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

            startActivityForResult(intent, ACTIVITY_CROP_IMAGE_CAMERA);
        } catch (ActivityNotFoundException e ) {

        } catch (Exception e) {

        }
    }

    public void openImage(){
        try {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, ACTIVITY_CROP_IMAGE_GALLERY);
        } catch (ActivityNotFoundException e) {

        }
    }

    private void startDialog() {

        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        openImage();
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        capturePhoto(ACTIVITY_CROP_IMAGE_FILENAME);
                    }
                });
        myAlertDialog.show();
    }

}
