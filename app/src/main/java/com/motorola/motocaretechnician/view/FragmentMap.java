package com.motorola.motocaretechnician.view;

import android.location.Location;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.MapView;
import com.androidmapsextensions.Marker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.MarkerFactory;
import com.motorola.motocaretechnician.control.PositionManager;
import com.motorola.motocaretechnician.interfaces.ILocationTracker;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentMap extends BaseFragment implements ILocationTracker {

    @InjectView(R.id.mapView)
    MapView mapView;

    @InjectView(R.id.rdPendents)
    Button rdPendents;

    @InjectView(R.id.rdAccepted)
    Button rdAccepted;

    @InjectView(R.id.ibZoomIn)
    ImageButton ibZoomIn;

    @InjectView(R.id.ibZoomOut)
    ImageButton ibZoomOut;

    @InjectView(R.id.ibMyPosition)
    ImageButton ibMyPosition;

    private GoogleMap googleMap;
    private Handler handlerMap;
    private boolean mapLoaded;
    private Location currentLocation;

    private boolean moveCamera = true;

    private RealmResults<MotoRequest> motoRequests;

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if(event.getKey() == GenericBus.REQUEST_LIST_REFRESH) {
            googleMap.clear();
            addAppointmentMarkersOnMap(motoRequests.subList(0, motoRequests.size()));
        }
    }

    @OnClick(R.id.rdPendents)
    void onRdPendentsClicked(){

        EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, 0, null));
    }
    @OnClick(R.id.rdAccepted)
    void onRdAcceptedClicked(){

        EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, 1, null));
    }


    @OnClick(R.id.ibZoomIn)
    void onZoomInClick(){
        if(googleMap != null) {
            googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        }

    }

    @OnClick(R.id.ibZoomOut)
    void onZoomOutClick(){
        if(googleMap != null) {
            googleMap.animateCamera(CameraUpdateFactory.zoomOut());
        }
    }

    @OnClick(R.id.ibMyPosition)
    void onMyPositionClick(){
        if(googleMap != null) {
            zoomInMyPosition();
        }
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_requests_tab_map;
    }

    @Override
    protected void doOnCreated(View view) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
        if(status == ConnectionResult.SUCCESS) {
            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
                PositionManager.getInstance().initialize(this, getActivity());


                Realm realm = Realm.getInstance(getActivity());
                motoRequests = realm.where(MotoRequest.class).isNotNull("user").findAll();

                ibZoomIn.setEnabled(true);
                ibZoomOut.setEnabled(true);
                ibMyPosition.setEnabled(true);

                handlerMap = new Handler();
                handlerMap.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setUpMap();
                    }
                }, 500);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), status);
        }
    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    public void setUpMap() {
        mapView.onCreate(getSavedInstanceState());
        mapView.onResume();
        googleMap = mapView.getExtendedMap();
        addAppointmentMarkersOnMap(motoRequests.subList(0, motoRequests.size()));
        mapView.onResume();
        mapLoaded = true;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude), 16);
                googleMap.animateCamera(cameraUpdate);

                MotoRequest motoRequest = marker.getData();
                int tabPosition = motoRequest.getStatus().equals(Const.APPOINTMENT_PENDING) ? 0 : 1;

                EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, tabPosition, null));
                EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_SCROLL, motoRequest, null));
                return false;
            }
        });

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

            }
        });

        ibZoomIn.setEnabled(true);
        ibZoomOut.setEnabled(true);
        ibMyPosition.setEnabled(true);

    }

    public void addAppointmentMarkersOnMap(List<MotoRequest> motoRequests) {
        for(MotoRequest motoRequest : motoRequests) {
            googleMap.addMarker(MarkerFactory.getInstance().getMarker(motoRequest)).setData(motoRequest);
        }
    }

    public void zoomInMyPosition() {
        if(googleMap != null && currentLocation != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 14);
            googleMap.animateCamera(cameraUpdate);
            moveCamera = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            moveCamera = true;
            if(mapLoaded) {
                mapView.onResume();
            }
        }catch (Exception e){
            Log.e("ERROR!", "MESSAGE --> onResume map: ".concat(e.toString()));
        }
    }

    @Override
    public void onConnected() {
        currentLocation = PositionManager.getInstance().getLocation();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = PositionManager.getInstance().getLocation();
        if(currentLocation == null) {
            currentLocation = location;
        }
        if(moveCamera && googleMap != null) {
            zoomInMyPosition();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
