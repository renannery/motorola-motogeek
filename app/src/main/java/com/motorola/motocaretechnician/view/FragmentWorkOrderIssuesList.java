package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoIssues;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryIssue;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Issue;

import java.util.ArrayList;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentWorkOrderIssuesList extends BaseFragment {

    @InjectView(R.id.rvIssues)
    RecyclerView rvIssues;

    private RecyclerView.LayoutManager mLayoutManager;
    private AdapterMotoIssues mAdapter;
    private ArrayList<Issue> selectedIssues;

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_issues_list;
    }

    @Override
    protected void doOnCreated(View view) {

        rvIssues.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvIssues.setLayoutManager(mLayoutManager);
        selectedIssues = new ArrayList<>();

        Realm realm = Realm.getInstance(getActivity());

        Bundle bundle = getArguments();
        long categoryId = bundle.getLong(Const.ISSUE_ID);

        CategoryIssue categoryIssue = realm.where(CategoryIssue.class).equalTo("code", categoryId).findFirst();

        mAdapter = new AdapterMotoIssues(getActivity(), categoryIssue.getIssues());
        rvIssues.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                Issue issue = (Issue) object;
                selectedIssues = Utils.getInstance().getWorkOrderSelectedIssues();
                if (selectedIssues == null || !selectedIssues.contains(issue)) {
                    EventBus.getDefault().post(new GenericBus(GenericBus.ADD_ISSUES, issue, null));
                    getFragmentManager().popBackStack(FragmentCreateWorkOrder.class.getName(), 0);
                } else {
                    Toast.makeText(getActivity(), R.string.create_work_order_issue_already_selected, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(getResources().getString(R.string.quotation_select_issue));
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
