package com.motorola.motocaretechnician.view;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.ServerCentral;
import com.motorola.motocaretechnician.control.SessionManager;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.User;
import com.motorola.motocaretechnician.network.JsonCentral;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.mime.TypedFile;

public class ActivityProfile extends BaseActivity {

    private static final int ACTIVITY_PROFILE = 0xabc;

    User user;

    @InjectView(R.id.ivProfilePhoto)
    ImageView ivProfilePhoto;

    @InjectView(R.id.tvName)
    TextView tvName;

    @InjectView(R.id.tvPhone)
    TextView tvPhone;

    @InjectView(R.id.tvAddress)
    TextView tvAddress;

    @InjectView(R.id.tvWorkHour)
    TextView tvWorkHour;

    @InjectView(R.id.tvQualifications)
    TextView tvQualifications;

    @InjectView(R.id.rbProfileRatingBar)
    RatingBar rbProfileRatingBar;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.ivProfilePhoto)
    public void onPhotoClick() {
        startActivity(CallManager.imageCropper(this));
    }

   /* @OnClick(R.id.btProfileSave)
    public void onProfileSaveClick() {
        JsonObject jsonUserParameters = new JsonObject();
        jsonUserParameters.addProperty("firstName", metProfileFirstName.getText().toString());
        jsonUserParameters.addProperty("lastName", metProfileLastName.getText().toString());
        jsonUserParameters.addProperty("address", metProfileAddress.getText().toString());
        jsonUserParameters.addProperty("city", metProfileCity.getText().toString());
        jsonUserParameters.addProperty("zipcode", metProfileZipcode.getText().toString());
        jsonUserParameters.addProperty("phone", metProfilePhone.getText().toString());
        jsonUserParameters.addProperty("rating", rbProfileRatingBar.getRating());
        jsonUserParameters.addProperty("estimatedTime", tvProfileEstimatedTime.getText().toString());
        jsonUserParameters.addProperty("workHour", tvProfileWorkHour.getText().toString());
        jsonUserParameters.addProperty("status", switchProfileStatus.isChecked());
        jsonUserParameters.addProperty("about", metProfileAbout.getText().toString());

        JsonCentral.getInstance().profileUser(jsonUserParameters);
        finish();

    }*/

    @OnClick(R.id.llWorkHourEdit)
    public void onProfileWorkHourClick() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePickerStart;
        final TimePickerDialog mTimePickerEnd;

        mTimePickerEnd = new TimePickerDialog(ActivityProfile.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (timePicker.isShown()) {
                    String startTime = tvWorkHour.getText().toString();

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR, selectedHour);
                    String strHrsToShow = (calendar.get(Calendar.HOUR) == 0) ?"12":calendar.get(Calendar.HOUR)+"";
                    String workSchedule = startTime.concat(" - " + strHrsToShow + (calendar.get(Calendar.AM_PM)==1?"am":"pm"));
                    tvWorkHour.setText(workSchedule);
                }
            }
        }, hour, minute, false);
        mTimePickerEnd.setTitle("End Work Time");

        mTimePickerStart = new TimePickerDialog(ActivityProfile.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (timePicker.isShown()) {

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR, selectedHour);
                    String strHrsToShow = (calendar.get(Calendar.HOUR) == 0) ?"12":calendar.get(Calendar.HOUR)+"";
                    tvWorkHour.setText(strHrsToShow + (calendar.get(Calendar.AM_PM)==1?"am":"pm"));
                    mTimePickerEnd.show();
                }
            }
        }, hour, minute, false);
        mTimePickerStart.setTitle("Start Work Time");
        mTimePickerStart.show();
    }

   /* @OnClick(R.id.btnProfileService)
    public void onProfileServiceClick() {

        EditText myEditText = new EditText(this);
        myEditText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        new MaterialDialog.Builder(this)
                .title("Services")
                .customView(R.layout.dialog_profile_services, false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        super.onNeutral(dialog);
                    }
                })
                .positiveText(getString(R.string.dialog_ok))
                .negativeText(getString(R.string.dialog_cancel))
                .show();
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.USER_PROFILE_IMAGE_CROP) {
            String croppedImagePath = (String) event.getObject();
            File file = new File(croppedImagePath);
            Picasso.with(this).invalidate(file);
            Picasso.with(this)
                    .load(file)
                    .fit()
                    .centerCrop()
                    .into(ivProfilePhoto);
            JsonCentral.getInstance().uploadProfileImage(user.getId(), new TypedFile("image/jpeg", file));
            Toast.makeText(this, getString(R.string.activity_profile_toast_upload_image), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(this.getString(R.string.toolbar_profile));
    }

    @Override
    public int layoutToInflate() {
        return R.layout.activity_profile;
    }

    @Override
    protected void doOnFirstTime() {}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case ACTIVITY_PROFILE:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();

                    Picasso.with(this)
                            .load(selectedImage)
                            .fit()
                            .centerCrop()
                            .into(ivProfilePhoto);

                }
        }
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        user = new User();
        user = SessionManager.getInstance().getCurrentUser();

        Picasso.with(this)
                .load(ServerCentral.getInstance().getImageWithPath(this, user.getPhoto()))
                .fit()
                .centerCrop()
                .into(ivProfilePhoto);


        tvName.setText(user.getFirstName() + " " + user.getLastName());

        tvAddress.setText(user.getAddress() + " - " + user.getCity() + " - SP");

        tvPhone.setText(user.getPhone().replace("+55 ",""));

        tvWorkHour.setText(user.getWorkHour().replace(":00 ",""));

        rbProfileRatingBar.setRating(3.5f);

    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }
}
