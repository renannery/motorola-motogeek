package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoWorkOrderIssuesSelected;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.EditTextValidator;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.model.Accessory;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Issue;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.MotoWorkOrder;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentCreateWorkOrder extends BaseFragment {
    @InjectView(R.id.rvIssues)
    RecyclerView rvIssues;

    @InjectView(R.id.metAccessoriesDetails)
    MaterialEditText metAccessoriesDetails;

    @InjectView(R.id.metSpecialProject)
    MaterialEditText metSpecialProject;

    @InjectView(R.id.metProductAppearance)
    MaterialEditText metProductAppearance;

    @InjectView(R.id.metLoanedDevice)
    MaterialEditText metLoanedDevice;

    @InjectView(R.id.metLoanedDeviceSerial)
    MaterialEditText metLoanedDeviceSerial;

    @InjectView(R.id.metEstimatedTimeRepair)
    MaterialEditText metEstimatedTimeRepair;

    @InjectView(R.id.spinnerCarriers)
    Spinner spinnerCarriers;

    @InjectView(R.id.checkboxNone)
    CheckBox checkboxNone;

    @InjectView(R.id.checkboxCable)
    CheckBox checkboxCable;

    @InjectView(R.id.checkboxSimCard)
    CheckBox checkboxSimCard;

    @InjectView(R.id.checkboxCharger)
    CheckBox checkboxCharger;

    @InjectView(R.id.checkboxMemoryCard)
    CheckBox checkboxMemoryCard;

    @InjectView(R.id.checkboxHeadphones)
    CheckBox checkboxHeadphones;

    @InjectView(R.id.checkboxBattery)
    CheckBox checkboxBattery;

    @InjectView(R.id.checkboxOthers)
    CheckBox checkboxOthers;

    @InjectView(R.id.checkboxDeviceAlertList)
    CheckBox checkboxDeviceAlertList;

    @InjectView(R.id.checkboxManualSerialNumberValidation)
    CheckBox checkboxManualSerialNumberValidation;

    @InjectView(R.id.checkboxDeviceForLoan)
    CheckBox checkboxDeviceForLoan;

    ArrayList<Accessory> accessories;

    private RecyclerView.LayoutManager mLayoutManager;
    private AdapterMotoWorkOrderIssuesSelected mIssuesAdapter;
    private int recyclerViewHeight;
    private ArrayList<Issue> selectedIssues;
    private long motoRequestId;

    @OnClick(R.id.llIssues)
    public void onIssuesClick() {
        Fragment fragmentIssues = CallManager.fragmentWorkOrderIssuesCategoriesList();
        ((ActivityRequestDetails) getActivity()).setFragmentRequestDetails(fragmentIssues, true);
    }

    @OnClick(R.id.llOpenWorkOrder)
    public void onOpenWorkOrderClick() {
        if(validateAllTextViews()) {
            getAllCheckedItems();
            createWorkOrder();
        }
    }

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_create_work_order;
    }

    @Override
    public void doOnCreated(View view) {
        Bundle b = getArguments();
        motoRequestId = b.getLong(Const.MOTO_REQUEST_ID);

        accessories = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvIssues.setLayoutManager(mLayoutManager);
        selectedIssues = new ArrayList<>();

        metAccessoriesDetails.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metSpecialProject.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metProductAppearance.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metLoanedDevice.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metLoanedDeviceSerial.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metEstimatedTimeRepair.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));

    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(getActivity().getString(R.string.request_details_create_work_order));
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.ADD_ISSUES) {
            Issue issue = (Issue) event.getObject();
            selectedIssues.add(issue);
            Utils.getInstance().setWorkOrderSelectedIssues(selectedIssues);

            mIssuesAdapter = new AdapterMotoWorkOrderIssuesSelected(selectedIssues);
            recyclerViewHeight = Utils.getInstance().resizeRecyclerViewHeight(
                    getResources().getDimensionPixelSize(R.dimen.button_height) +
                            getResources().getDimensionPixelSize(R.dimen.margin_bottom_selected_issue_part),
                    mIssuesAdapter.getItemCount());
            rvIssues.setAdapter(mIssuesAdapter);
            rvIssues.getLayoutParams().height = recyclerViewHeight;
        }

        if (event.getKey() == GenericBus.UPDATE_WORK_ORDER_ISSUES) {
            mIssuesAdapter.notifyDataSetChanged();
            recyclerViewHeight = Utils.getInstance().resizeRecyclerViewHeight(
                    getResources().getDimensionPixelSize(R.dimen.button_height) +
                            getResources().getDimensionPixelSize(R.dimen.margin_bottom_selected_issue_part),
                    mIssuesAdapter.getItemCount());
            rvIssues.getLayoutParams().height = recyclerViewHeight;
        }
    }

    public void getAllCheckedItems() {
        accessories.clear();
        if(checkboxNone.isChecked()) {
            accessories.add(new Accessory(checkboxNone.getId(), checkboxNone.getText().toString()));
        }

        if(checkboxCable.isChecked()) {
            accessories.add(new Accessory(checkboxCable.getId(), checkboxCable.getText().toString()));
        }

        if(checkboxSimCard.isChecked()) {
            accessories.add(new Accessory(checkboxSimCard.getId(), checkboxSimCard.getText().toString()));
        }

        if(checkboxCharger.isChecked()) {
            accessories.add(new Accessory(checkboxCharger.getId(), checkboxCharger.getText().toString()));
        }

        if(checkboxMemoryCard.isChecked()) {
            accessories.add(new Accessory(checkboxMemoryCard.getId(), checkboxMemoryCard.getText().toString()));
        }

        if(checkboxHeadphones.isChecked()) {
            accessories.add(new Accessory(checkboxHeadphones.getId(), checkboxHeadphones.getText().toString()));
        }

        if(checkboxBattery.isChecked()) {
            accessories.add(new Accessory(checkboxBattery.getId(), checkboxBattery.getText().toString()));
        }

        if(checkboxOthers.isChecked()) {
            accessories.add(new Accessory(checkboxOthers.getId(), checkboxOthers.getText().toString()));
        }

        if(checkboxDeviceAlertList.isChecked()) {
            accessories.add(new Accessory(checkboxDeviceAlertList.getId(), checkboxDeviceAlertList.getText().toString()));
        }

        if(checkboxManualSerialNumberValidation.isChecked()) {
            accessories.add(new Accessory(checkboxManualSerialNumberValidation.getId(), checkboxManualSerialNumberValidation.getText().toString()));
        }

        if(checkboxDeviceForLoan.isChecked()) {
            accessories.add(new Accessory(checkboxDeviceForLoan.getId(), checkboxDeviceForLoan.getText().toString()));
        }

    }

    private boolean validateAllTextViews() {
        return metAccessoriesDetails.validate() && metSpecialProject.validate()
                && metProductAppearance.validate() && metLoanedDevice.validate()
                && metLoanedDeviceSerial.validate() && metEstimatedTimeRepair.validate();
    }

    private void createWorkOrder() {
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        MotoWorkOrder motoWorkOrder = realm.createObject(MotoWorkOrder.class);
        motoWorkOrder.setNumber("WO" + System.currentTimeMillis());
        motoWorkOrder.setStatusDescription("Waiting Analysis");
        motoWorkOrder.setStatusId(1);
        motoWorkOrder.setHwWarrantyStart("2014-04-01T08:41:51+00:00Z");
        motoWorkOrder.setHwWarrantyEnd("2014-04-01T08:41:51+00:00Z");
        motoWorkOrder.setSwWarrantyStart("2014-04-01T08:41:51+00:00Z");
        motoWorkOrder.setSwWarrantyEnd("2014-04-01T08:41:51+00:00Z");
        motoWorkOrder.setDueDate("2014-04-01T08:41:51+00:00Z");

        MotoRequest motoRequest = realm.where(MotoRequest.class).equalTo(Const.MOTO_REQUEST_ID, motoRequestId).findFirst();
        motoRequest.setWorkOrder(motoWorkOrder);
        realm.commitTransaction();
        realm.close();

        Bundle bundle = new Bundle();
        bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());
        startActivity(CallManager.workOrderDetail(getActivity()).putExtras(bundle));
        EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_LIST_REFRESH, null, null));
        getActivity().finish();
    }

    @Override
    protected boolean isDrawerLocked() {
        return true;
    }
}
