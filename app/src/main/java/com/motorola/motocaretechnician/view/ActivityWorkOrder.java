package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.model.Issue;
import com.motorola.motocaretechnician.model.Part;

import java.util.ArrayList;

/**
 * Created by jsilva on 5/4/15.
 */
public class ActivityWorkOrder extends BaseActivity {

    public ArrayList<Issue> issues = new ArrayList<Issue>();
    public ArrayList<Part> parts = new ArrayList<Part>();

    @Override
    protected void doOnFirstTime() {
        Fragment fragment = CallManager.fragmentWorkOrderDetails();

        setFragmentWorkOrderDetail(fragment, false);
    }

    @Override
    protected void doOnCreated(Bundle savedInstanceState) {

    }

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_work_order;
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }


    public void addIssue(Issue issue){
        issues.add(issue);
    }

    public void addPart(Part part){
        parts.add(part);
    }

}
