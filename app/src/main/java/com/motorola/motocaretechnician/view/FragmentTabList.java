package com.motorola.motocaretechnician.view;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterViewPagerRequestList;
import com.motorola.motocaretechnician.model.Appointment;
import com.motorola.motocaretechnician.model.GenericBus;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import io.realm.RealmResults;

/**
 * Created by jsilva on 4/17/15.
 */
public class FragmentTabList extends BaseFragment {

    ViewPager pager;
    AdapterViewPagerRequestList adapter;

    List<String> listTabs = new ArrayList<String>();


    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_request_tab_lists;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.CHANGE_MAP_TAB) {
            pager.setCurrentItem((Integer) event.getObject());
        }
    }

    @Override
    protected void doOnCreated(View view) {

        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        final CharSequence[] titles = listTabs.toArray(new CharSequence[listTabs.size()]);

        listTabs.add(getActivity().getString(R.string.request_map_tab_pending));
        listTabs.add(getActivity().getString(R.string.request_map_tab_accepted));

        adapter =  new AdapterViewPagerRequestList(getActivity().getSupportFragmentManager(), titles, listTabs.size());

        pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(adapter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
