package com.motorola.motocaretechnician.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.EditTextValidator;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Result;
import com.motorola.motocaretechnician.network.JsonCentral;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class ActivityForgotPassword extends BaseActivity {

    private ProgressDialog progressDialog;


    @InjectView(R.id.metEmail)
    MaterialEditText metEmail;

    @InjectView(R.id.llSendForgotPasswordRequest)
    LinearLayout llSendForgotPasswordRequest;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.llSendForgotPasswordRequest)
    public void onSendMailClick() {

        if (!metEmail.validate()) {
            return;
        }

        JsonObject jsonUserParameters = new JsonObject();
        jsonUserParameters.addProperty("email", metEmail.getText().toString().toLowerCase());

        JsonCentral.getInstance().forgotPassword(jsonUserParameters);
        progressDialog = Utils.getInstance().createProgressDialog(this, getString(R.string.dialog_forgot_password_title), getString(R.string.dialog_forgot_password_content));
    }

    @Override
    public int layoutToInflate() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected void doOnFirstTime() {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(this.getString(R.string.toolbar_login));

        metEmail.addValidator(new RegexpValidator(getResources().getString(R.string.login_email_crouton_message_error), Patterns.EMAIL_ADDRESS.pattern()));
        metEmail.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
    }


    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.USER_FORGOT_PASSWORD) {
            Result result = event.getResult();

            if (result != null && result.isSuccess()) {
                Utils.getInstance().finalizeProgressDialog(progressDialog);
                Toast.makeText(this, getString(R.string.activity_forgot_password_reset_ok), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Utils.getInstance().finalizeProgressDialog(progressDialog);
            }
        }
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        metEmail.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }

}