package com.motorola.motocaretechnician.view;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.EditTextValidator;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.User;
import com.motorola.motocaretechnician.network.JsonCentral;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentLogin extends BaseFragment {

    private ProgressDialog progressDialog;

    @InjectView(R.id.metUsername)
    MaterialEditText metUsername;

    @InjectView(R.id.metPassword)
    MaterialEditText metPassword;

    @InjectView(R.id.tvLoginTitle)
    TextView tvLoginTitle;

    @InjectView((R.id.llLoginButton))
    LinearLayout llLoginButton;

    @InjectView((R.id.llForgotPasswordButton))
    LinearLayout llForgotPasswordButton;

    @InjectView(R.id.llJoinUsButton)
    LinearLayout llJoinUsButton;

    @OnClick(R.id.llForgotPasswordButton)
    public void onForgotPasswordClick() {
        startActivity(CallManager.forgotPassword(getActivity()));
    }

    @OnClick(R.id.llLoginButton)
    public void onLoginClick() {

        metPassword.validate();

        if (metUsername.validate() && metPassword.validate()) {

            JsonObject jsonUserParameters = new JsonObject();
            jsonUserParameters.addProperty("username", metUsername.getText().toString().toLowerCase());
            jsonUserParameters.addProperty("pwd", metPassword.getText().toString().toLowerCase());

            progressDialog = Utils.getInstance().createProgressDialog(getActivity(), getActivity().getString(R.string.dialog_login_title), getActivity().getString(R.string.dialog_login_content));
            JsonCentral.getInstance().loginUser(jsonUserParameters);
        }
    }

    @OnClick(R.id.llJoinUsButton)
    public void onJoinUsClick() {
        startActivity(CallManager.joinUs(getActivity()));
    }

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_login;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void doOnCreated(View view) {
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"fonts/moto_sans_regular.otf");
        tvLoginTitle.setTypeface(type);

        metUsername.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        metUsername.addValidator(new RegexpValidator(getResources().getString(R.string.login_email_crouton_message_error), Patterns.EMAIL_ADDRESS.pattern()));
        metPassword.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(getActivity().getString(R.string.toolbar_login));
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.USER_LOGIN) {
            if (event.getResult().isSuccess()) {
                User user = (User) event.getObject();

                if (user != null) {
                    Realm realm = Realm.getInstance(getActivity());
                    realm.beginTransaction();
                    User realmUser = realm.copyToRealmOrUpdate(user);
                    realm.commitTransaction();
                    realm.close();

                    Utils.getInstance().finalizeProgressDialog(progressDialog);
                    goToMainScreen();
                } else {
                    Toast.makeText(getActivity(), event.getResult().getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getActivity(), event.getResult().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void goToMainScreen() {
        startActivity(CallManager.main(getActivity()));
        getActivity().finish();
    }

    @Override
    protected boolean isDrawerLocked() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
