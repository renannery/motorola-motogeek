package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoPendingRequests;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.ViewInfo;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentPendingRequestsList extends BaseFragment {

    @InjectView(R.id.rvPendingRequests)
    RecyclerView rvPendingRequests;

    private RecyclerView.LayoutManager mLayoutManager;
    private AdapterMotoPendingRequests mAdapter;

    @Override
    protected int layoutToInflate() {
        return R.layout.tab_pending_requests;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.REQUEST_SCROLL) {
            MotoRequest motoRequest = (MotoRequest) event.getObject();
            if (motoRequest.getStatus().equals(Const.APPOINTMENT_PENDING)) {
                for (int i = 0; i < mAdapter.getItemCount(); i++) {
                    MotoRequest motoRequestTarget = mAdapter.getItems().get(i);
                    if (motoRequestTarget.getId() == motoRequest.getId()) {
                        rvPendingRequests.smoothScrollToPosition(i);
                        break;
                    }
                }
            }
        }

        if(event.getKey() == GenericBus.REQUEST_LIST_REFRESH) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void doOnCreated(View view) {
        rvPendingRequests.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvPendingRequests.setLayoutManager(mLayoutManager);

        Realm realm = Realm.getInstance(getActivity());
        RealmResults<MotoRequest> motoRequests = realm.where(MotoRequest.class).equalTo(Const.APPOINTMENT_STATUS_FIELD, Const.APPOINTMENT_PENDING).findAll();
        mAdapter = new AdapterMotoPendingRequests(getActivity(), motoRequests);
        rvPendingRequests.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                MotoRequest motoRequest = (MotoRequest) object;
                ImageView ivClientPhoto = (ImageView) view.findViewById(R.id.ivClientPhoto);
                ViewInfo viewInfo = new ViewInfo(ivClientPhoto);
                Bundle bundle = new Bundle();
                bundle.putParcelable(ViewInfo.VIEW_INFO, viewInfo);
                bundle.putInt(Const.REQUEST_DETAIL_FRAGMENT_TO_INFLATE, Const.TYPE_PENDING_REQUEST_LIST);
                bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());

                startActivity(CallManager.requestDetails(getActivity()).putExtras(bundle));
                getActivity().overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
