package com.motorola.motocaretechnician.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;

import butterknife.InjectView;
import butterknife.OnClick;

public class ActivityJoinUs extends BaseActivity {

    @InjectView(R.id.header)
    TextView header;

    @InjectView(R.id.btGoMotorolaWebsite)
    Button btGoMotorolaWebsite;

    @OnClick(R.id.btGoMotorolaWebsite)
    public void onGotoMotorolaWebSiteClick() {
        startActivity(CallManager.goToWebsite("http://www.motorola.com/us/consolidated-apps-page/moto-care.html"));
    }

    @Override
    public int layoutToInflate() {
        return R.layout.activity_join_us;
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/moto_sans_regular.otf");
        header.setTypeface(type);
    }

    @Override
    protected void doOnFirstTime() {

    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }

}