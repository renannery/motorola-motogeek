package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoMilestones;
import com.motorola.motocaretechnician.control.AdapterMotoParts;
import com.motorola.motocaretechnician.control.AdapterQuotation;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.DateUtils;
import com.motorola.motocaretechnician.control.ServerCentral;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.control.XMPPUtils;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.MotoWorkOrder;
import com.motorola.motocaretechnician.model.Part;
import com.motorola.motocaretechnician.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by jsilva on 4/27/15.
 */
public class FragmentWorkOrderDetails extends BaseFragment {

    @InjectView(R.id.tvSerialNumber)
    TextView tvSerialNumber;

    @InjectView(R.id.tvDevice)
    TextView tvDevice;

    @InjectView(R.id.tvUsername)
    TextView tvUsername;

    @InjectView(R.id.tvCityStateAppointment)
    TextView tvCityStateAppointment;

    @InjectView(R.id.ivClientPhoto)
    ImageView ivClientPhoto;

    @InjectView(R.id.tvWarrantyLabel)
    TextView tvWarrantyLabel;

    @InjectView(R.id.tvPrice)
    TextView tvPrice;

    @InjectView(R.id.tvStatus)
    TextView tvStatus;

    @InjectView(R.id.llQuotation)
    LinearLayout llQuotation;

    @InjectView(R.id.llWarranty)
    LinearLayout llWarranty;

    @InjectView(R.id.llQuotationBlock)
    LinearLayout llQuotationBlock;

    @InjectView(R.id.llAppointmentBlock)
    LinearLayout llAppointmentBlock;

    @InjectView(R.id.llDeviceBlock)
    LinearLayout llDeviceBlock;

    @InjectView(R.id.llDeliveryBlock)
    LinearLayout llDeliveryBlock;

    @InjectView(R.id.rvQuotation)
    RecyclerView rvQuotation;

    private MotoRequest motoRequest;
    private AdapterQuotation mAdapter;

    private int recyclerViewHeight;
    private float quotationTotal;


    public boolean blWarranty;

    @InjectView(R.id.rvMilestone)
    RecyclerView rvMilestone;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoMilestones mAdapterMilestones;

    @OnClick(R.id.llDelivery)
    public void onDeliveryBlockClick(){
        DeliveryDialog deliveryDialog = new DeliveryDialog();
        deliveryDialog.setCustomDialogCallback(new DeliveryDialog.CustomDialogButtonsCallback() {
            @Override
            public void doOkConfirmClick() {
                Realm realm = Realm.getInstance(getActivity());
                try {
                    realm.beginTransaction();
                    motoRequest.getWorkOrder().setStatusId(7);
                    motoRequest.getWorkOrder().setStatusDescription("Done");
                    realm.commitTransaction();
                    realm.close();
                    EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_LIST_REFRESH, motoRequest, null));
                    getActivity().finish();
                } catch (Exception e) {
                    realm.cancelTransaction();
                    realm.close();
                    EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_LIST_REFRESH, motoRequest, null));
                }

            }
        });
        deliveryDialog.show(getFragmentManager(), "dialog");
    }

    @OnClick(R.id.llQuotation)
    public void onQuotationClick() {
        ((ActivityWorkOrder)getActivity()).setFragmentWorkOrderDetail(CallManager.fragmentQuotationEdit(), true);
    }

    @OnClick(R.id.llWarranty)
    public void onWarrantyClick() {
        ((ActivityWorkOrder)getActivity()).setFragmentWorkOrderDetail(CallManager.fragmentQuotationEdit(), true);
    }

    @OnClick(R.id.llCall)
    public void onCallClick() {
        startActivity(CallManager.dialNumberToCall(motoRequest.getUser().getPhone()));
    }

    @OnClick(R.id.llChat)
    public void onChatClick(){

        XMPPUtils.getInstance(getActivity()).connect();
        XMPPUtils.getInstance(getActivity()).login();
        Realm realm = Realm.getInstance(getActivity());
        User user = realm.where(User.class).findFirst();


        Bundle bundle = new Bundle();
        bundle.putString(Const.USER_USERNAME, user.getUsername());
        startActivity(CallManager.startChat(getActivity()).putExtras(bundle));
    }

    @OnClick(R.id.llEditQuotation)
    public void onEditQuotationClick(){
        ((ActivityWorkOrder)getActivity()).setFragmentWorkOrderDetail(CallManager.fragmentQuotationEdit(), true);
    }

    @OnClick(R.id.llSendQuotation)
    public void onSendQuotationClick(){
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        if(blWarranty){
            motoRequest.getWorkOrder().setStatusDescription("Waiting Parts");
            motoRequest.getWorkOrder().setStatusId(Const.WAITING_PARTS);
        }else{
            motoRequest.getWorkOrder().setStatusDescription("Waiting approval");
            motoRequest.getWorkOrder().setStatusId(Const.WAITING_APPROVAL);
        }
        realm.commitTransaction();
        verifyWorkOrderStatus();
        fillFields();
    }

    @OnClick(R.id.llAppointment)
    public void onEditAppointmentClick(){
        Bundle bundle = new Bundle();
        bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());

        Fragment fragment = CallManager.fragmentWorkOrderAppointmentEdit();
        fragment.setArguments(bundle);

        ((ActivityWorkOrder)getActivity()).setFragmentWorkOrderDetail(fragment, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        verifyWorkOrderStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if(event.getKey() == GenericBus.BUTTON_IN_WARRANTY) {

        } else if(event.getKey() == GenericBus.BUTTON_CREATE_QUOTATION) {
            ArrayList<Part> parts = new ArrayList<>();
            parts.addAll(event.getObjects());

            mAdapter = new AdapterQuotation(parts);
            recyclerViewHeight = Utils.getInstance().resizeRecyclerViewHeight(
                    getResources().getDimensionPixelSize(R.dimen.quotation_height_adapter),
                    mAdapter.getItemCount());
            rvQuotation.setAdapter(mAdapter);
            rvQuotation.getLayoutParams().height = recyclerViewHeight;

            Realm realm = Realm.getInstance(getActivity());
            realm.beginTransaction();
            quotationTotal = 0;
            motoRequest.getWorkOrder().getParts().clear();
            RealmList<Part> realmList = new RealmList<>();
            realmList.addAll(parts);
            motoRequest.getWorkOrder().setParts(realmList);
            realm.commitTransaction();
            realm.close();
            sumPartsPrices(motoRequest.getWorkOrder().getParts());
        } else if(event.getKey() == GenericBus.BUTTON_SEND_QUOTATION) {

        }
    }

    public void getQuotation() {
        ArrayList<Part> parts = new ArrayList<>();
        parts.addAll(motoRequest.getWorkOrder().getParts());

        mAdapter = new AdapterQuotation(parts);
        recyclerViewHeight = Utils.getInstance().resizeRecyclerViewHeight(
                getResources().getDimensionPixelSize(R.dimen.quotation_height_adapter),
                mAdapter.getItemCount());
        rvQuotation.setAdapter(mAdapter);
        rvQuotation.getLayoutParams().height = recyclerViewHeight;

        sumPartsPrices(motoRequest.getWorkOrder().getParts());
    }

    private void sumPartsPrices(RealmList<Part> parts) {

        for(Part part: parts) {
            quotationTotal += part.getPrice();
        }
        tvPrice.setText(String.format("%.2f", quotationTotal));
    }

    public void verifyWorkOrderStatus() {
        if(motoRequest.getWorkOrder() != null) {
            switch (motoRequest.getWorkOrder().getStatusId()) {
                case Const.WAITING_ANALYSIS:
                    setupWaitingAnalysis();
                    break;
                case Const.WAITING_APPROVAL:
                    setupWaitingApproval();
                    break;
                case Const.QUOTE_REJECTED:
                    setupQuoteRejected();
                    break;
                case Const.WAITING_PARTS:
                    setupWaitingParts();
                    break;
                case Const.WAITING_REPAIR:
                    setupWaitingRepair();
                    break;
                case Const.REPAIRED:
                    setupRepaired();
                    break;
                case Const.DONE:
                    setupDone();
                    break;
                default:
                    break;
            }
        }else{
            setupWaitingAnalysis();
        }
    }

    private void setupWaitingAnalysis(){
        llAppointmentBlock.setVisibility(View.GONE);

        if(motoRequest.getWorkOrder().getParts().size() > 0) {
            llQuotationBlock.setVisibility(View.VISIBLE);
        } else {
            llQuotationBlock.setVisibility(View.GONE);
        }

    }

    private void setupWaitingApproval(){
        blWarranty = false;
        llQuotation.setVisibility(View.GONE);
        llWarranty.setVisibility(View.GONE);
        tvWarrantyLabel.setVisibility(View.VISIBLE);
        llQuotationBlock.setVisibility(View.VISIBLE);
        llAppointmentBlock.setVisibility(View.GONE);
    }

    private void setupQuoteRejected(){

    }

    private void setupWaitingParts(){
        blWarranty = true;
        llQuotation.setVisibility(View.GONE);
        llWarranty.setVisibility(View.GONE);
        tvWarrantyLabel.setVisibility(View.VISIBLE);
        llQuotationBlock.setVisibility(View.VISIBLE);
        llAppointmentBlock.setVisibility(View.GONE);
    }

    private void setupWaitingRepair(){
        llAppointmentBlock.setVisibility(View.GONE);
    }

    private void setupRepaired(){
        llQuotation.setVisibility(View.GONE);
        llWarranty.setVisibility(View.GONE);
        tvWarrantyLabel.setVisibility(View.VISIBLE);
        llQuotationBlock.setVisibility(View.GONE);
        llAppointmentBlock.setVisibility(View.GONE);

    }

    private void setupDone(){

    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_work_order_detail;
    }

    @Override
    protected void doOnCreated(View view) {

        rvMilestone.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvMilestone.setLayoutManager(mLayoutManager);

        rvQuotation.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvQuotation.setLayoutManager(mLayoutManager);

        Bundle b = getActivity().getIntent().getExtras();
        long workOrderNumber = b.getLong(Const.MOTO_REQUEST_ID);

        Realm realm = Realm.getInstance(getActivity());
        motoRequest = realm.where(MotoRequest.class).equalTo(Const.MOTO_REQUEST_ID, workOrderNumber).findFirst();

        if(motoRequest != null) {
            fillFields();
            if(motoRequest.getWorkOrder() !=null ) {
                mAdapterMilestones = new AdapterMotoMilestones(getActivity(), motoRequest.getWorkOrder().getMilestones());
                rvMilestone.setAdapter(mAdapterMilestones);
                resizeWorkOrderMilestoneRecycleView();
            }
        }
        realm.close();
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(this.getString(R.string.work_order_details_toolbar_title));
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    public void fillFields() {
        tvUsername.setText(String.valueOf(motoRequest.getUser().getFirstName().concat(" ").concat(motoRequest.getUser().getLastName())));
        tvCityStateAppointment.setText(motoRequest.getUser().getCity());
        tvSerialNumber.setText(motoRequest.getDevice().getSerialNumber());
        tvDevice.setText(motoRequest.getDevice().getModel());

        if(motoRequest.getWorkOrder() != null){
            tvStatus.setText(motoRequest.getWorkOrder().getStatusDescription());
            if(motoRequest.getWorkOrder().getParts() != null || !motoRequest.getWorkOrder().getParts().isEmpty()) {
                getQuotation();
            }
        }

       // validateWarranty(workOrder);

        Picasso.with(getActivity())
                .load(ServerCentral.getInstance().getImageWithPath(getActivity(), motoRequest.getUser().getPhoto()))
                .fit()
                .centerCrop()
                .into(ivClientPhoto);
    }

    private void validateWarranty(MotoWorkOrder workOrder){

        Date hardwareWarrantyEnd = DateUtils.convertStringToDate(workOrder.getHwWarrantyEnd());

        if(hardwareWarrantyEnd.compareTo(new Date()) >= 0){
            tvWarrantyLabel.setText(getResources().getString(R.string.in_warranty));
        }else{
            tvWarrantyLabel.setText(getResources().getString(R.string.out_of_warranty));
        }

    }

    public void resizeWorkOrderMilestoneRecycleView(){
        int viewMilestonesHeight = motoRequest.getWorkOrder().getMilestones().size()
                * (getResources().getDimensionPixelSize(R.dimen.button_height) + getResources().getDimensionPixelSize(R.dimen.margin_bottom_selected_issue_part));
        rvMilestone.getLayoutParams().height = viewMilestonesHeight;

        mAdapterMilestones.notifyDataSetChanged();

    }

}
