package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoIssues;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryIssue;
import com.motorola.motocaretechnician.model.Issue;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import io.realm.Realm;

/**
 * Created by jsilva on 5/6/15.
 */
public class FragmentIssuesList extends BaseFragment {

    @InjectView(R.id.rvIssues)
    RecyclerView rvIssues;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoIssues mAdapter;
    private List<Issue> partsIssues = new ArrayList<Issue>();


    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_issues_list;
    }

    @Override
    protected void doOnCreated(View view) {

        rvIssues.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvIssues.setLayoutManager(mLayoutManager);

        Realm realm = Realm.getInstance(getActivity());

        Bundle bundle = getArguments();
        Long categoryId = bundle.getLong(Const.ISSUE_ID);

        CategoryIssue categoryIssue = realm.where(CategoryIssue.class).equalTo("code", categoryId).findFirst();

        mAdapter = new AdapterMotoIssues(getActivity(), categoryIssue.getIssues());
        rvIssues.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                Issue issue = (Issue) object;
                ((ActivityWorkOrder)getActivity()).addIssue(issue);
                getFragmentManager().popBackStack(FragmentQuotationEdit.class.getName(), 0);

            }
        });


    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText("WO 123456");
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
