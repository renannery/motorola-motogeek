package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoPartsIssues;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryIssue;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.network.JsonCentral;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentIssuesCategoriesList extends BaseFragment {

    @InjectView(R.id.rvIssues)
    RecyclerView rvIssues;

    @InjectView(R.id.progressbar)
    ProgressBar progressbar;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoPartsIssues mAdapter;
    private List<CategoryIssue> partsIssues = new ArrayList<CategoryIssue>();

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_issues_categories_list;
    }

    @Override
    protected void doOnCreated(View view) {

        rvIssues.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvIssues.setLayoutManager(mLayoutManager);

        JsonCentral.getInstance().issuesCategories();

    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(getResources().getString(R.string.quotation_select_issue));
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.ISSUES_CATEGORIES_LIST) {
            if (event.getResult().isSuccess()) {
                ArrayList<CategoryIssue> categoriesItems = event.getObjects();
                if (categoriesItems != null) {
                    for(CategoryIssue category : categoriesItems){
                        Realm realm = Realm.getInstance(getActivity());
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(category);

                        partsIssues.add(category);

                        realm.commitTransaction();
                    }
                    progressbar.setVisibility(View.GONE);
                    mAdapter = new AdapterMotoPartsIssues(getActivity(), partsIssues);

                    rvIssues.setAdapter(mAdapter);

                    mAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onClickItem(View view, Object object) {
                            CategoryIssue issue = (CategoryIssue) object;
                            Bundle bundle = new Bundle();
                            bundle.putLong(Const.ISSUE_ID, issue.getCode());

                            Fragment fragment = CallManager.fragmentIssuesList();
                            fragment.setArguments(bundle);
                            ((ActivityWorkOrder) getActivity()).setFragmentIssues(fragment, true);
                        }
                    });

                }
            }
        }
    }
}
