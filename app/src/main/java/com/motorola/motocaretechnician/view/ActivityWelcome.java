package com.motorola.motocaretechnician.view;

import android.os.Bundle;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.UserUtils;


public class ActivityWelcome extends BaseActivity {

    @Override
    public int layoutToInflate() {
        return R.layout.activity_welcome;
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {

        if (UserUtils.getInstance().isUserLogged(this)) {
            goToMainScreen();
            return;
        }
        setFragmentWelcome(CallManager.fragmentWelcome(), false);
    }

    private void goToMainScreen() {
        startActivity(CallManager.main(this));
        finish();
    }

    @Override
    protected void doOnFirstTime() {

    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }
}