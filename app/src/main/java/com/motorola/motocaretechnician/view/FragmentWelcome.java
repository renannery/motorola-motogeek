package com.motorola.motocaretechnician.view;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;

import butterknife.InjectView;
import butterknife.OnClick;

public class FragmentWelcome extends BaseFragment {

    @InjectView(R.id.llStart)
    LinearLayout llStart;

    @OnClick(R.id.llStart)
    public void onStartClick() {
        ((ActivityWelcome) getActivity()).setFragmentWelcome(CallManager.fragmentDisclaimer(), false);
    }

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_welcome;
    }

    @Override
    public void doOnCreated(View view) {

    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return true;
    }
}
