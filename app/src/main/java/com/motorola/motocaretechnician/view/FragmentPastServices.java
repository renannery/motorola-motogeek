package com.motorola.motocaretechnician.view;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterPastServices;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.network.JsonCentral;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentPastServices extends BaseFragment {
    @InjectView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectView(R.id.rvPastServices)
    RecyclerView rvPastServices;

    @InjectView(R.id.progressbar)
    ProgressBar progressbar;

    @InjectView(R.id.ivSetaFiltro)
    ImageView ivSetaFiltro;

    private AdapterPastServices mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBarDrawerToggle drawerToggle;

    private SlidingUpPanelLayout slidingUpPanelLayout;

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_past_services;
    }

    @Override
    public void doOnCreated(View view) {
        JsonCentral.getInstance().pastServices(String.valueOf(UserUtils.getInstance().getLoggedUser(getActivity()).getId()));
        slidingUpPanelLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        rvPastServices.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvPastServices.setLayoutManager(mLayoutManager);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelExpanded(View panel) {
                ObjectAnimator.ofFloat(ivSetaFiltro, View.ROTATION, 0f, 180f).setDuration(150).start();
            }

            @Override
            public void onPanelCollapsed(View panel) {
                ObjectAnimator.ofFloat(ivSetaFiltro, View.ROTATION, 180f, 0f).setDuration(150).start();
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(getActivity().getString(R.string.past_services_toolbar_title));

        drawerToggle = new ActionBarDrawerToggle(getActivity(), ((ActivityMain) getActivity()).getDrawer(), toolbar, R.string.send, R.string.send);
        ((ActivityMain) getActivity()).setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void refreshItems() {
        JsonCentral.getInstance().openedServices(String.valueOf(UserUtils.getInstance().getLoggedUser(getActivity()).getId()));
    }

    private void onItemsLoadComplete() {
        progressbar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.USER_PAST_SERVICES) {
            if (event.getResult().isSuccess()) {
                ArrayList<MotoRequest> motoRequests =  event.getObjects();
                if (motoRequests != null) {
                    Realm realm = Realm.getInstance(getActivity());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(motoRequests);
                    realm.commitTransaction();
                    RealmResults<MotoRequest> motoRequestListResult = realm.where(MotoRequest.class).equalTo("workOrder.statusId",7).findAll();
                    mAdapter = new AdapterPastServices(getActivity(), motoRequestListResult);

                    mAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onClickItem(View view, Object object) {
                            MotoRequest motoRequest = (MotoRequest) object;

                            Bundle bundle = new Bundle();
                            bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());
                            startActivity(CallManager.workOrderDetail(getActivity()).putExtras(bundle));
                        }
                    });

                    rvPastServices.setAdapter(mAdapter);
                    realm.close();
                    onItemsLoadComplete();
                } else {
                    Log.e(getTag(), "null work order");
                }
            } else {
                Toast.makeText(getActivity(), event.getResult().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
