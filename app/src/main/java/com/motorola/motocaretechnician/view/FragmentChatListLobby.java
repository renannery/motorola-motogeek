package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterChatLobby;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.XMPPUtils;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.User;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentChatListLobby extends BaseFragment implements RosterListener{
    @InjectView(R.id.rvChatLobby)
    RecyclerView rvChatLobby;

    @InjectView(R.id.progressbar)
    ProgressBar progressbar;

    private AdapterChatLobby mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBarDrawerToggle drawerToggle;

    private ArrayList<User> chatContactList = new ArrayList<>();
    private Roster roster;

    @Override
    public int layoutToInflate() {
        return R.layout.fragment_chat_list;
    }

    @Override
    public void doOnCreated(View view) {
        XMPPUtils.getInstance(getActivity()).connect();
        XMPPUtils.getInstance(getActivity()).login();

        roster = Roster.getInstanceFor(XMPPUtils.getInstance(getActivity()).getConnection());
        roster.addRosterListener(this);

        rvChatLobby.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvChatLobby.setLayoutManager(mLayoutManager);

    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(getActivity().getString(R.string.chat_toolbar_title));

        drawerToggle = new ActionBarDrawerToggle(getActivity(), ((ActivityMain) getActivity()).getDrawer(), toolbar, R.string.send, R.string.send);
        ((ActivityMain) getActivity()).setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.CHAT_MESSAGE_BODY) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    @Override
    public void entriesAdded(Collection<String> addresses) {
        chatContactList.clear();
        Collection<RosterEntry> entries = roster.getEntries();
        for (RosterEntry entry : entries) {
            User user = new User(entry, Roster.getInstanceFor(XMPPUtils.getInstance(getActivity()).getConnection()).getPresence(entry.getUser()));
            chatContactList.add(user);
        }
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(chatContactList);
        realm.commitTransaction();
        realm.close();
        mAdapter = new AdapterChatLobby(getActivity(), chatContactList);
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                User user = (User) object;

                Bundle bundle = new Bundle();
                bundle.putString(Const.USER_USERNAME, user.getUsername());
                startActivity(CallManager.startChat(getActivity()).putExtras(bundle));
            }
        });
        rvChatLobby.post(new Runnable() {
            @Override
            public void run() {
                progressbar.setVisibility(View.GONE);
                rvChatLobby.setAdapter(mAdapter);
            }
        });
    }

    @Override
    public void entriesUpdated(Collection<String> addresses) {}

    @Override
    public void entriesDeleted(Collection<String> addresses) {}

    @Override
    public void presenceChanged(Presence presence) {}
}
