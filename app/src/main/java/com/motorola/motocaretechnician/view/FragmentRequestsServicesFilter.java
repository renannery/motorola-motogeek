package com.motorola.motocaretechnician.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.model.GenericBus;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

public class FragmentRequestsServicesFilter extends BaseFragment {

    @InjectView(R.id.llFilterContent)
    LinearLayout llFilterContent;

    @InjectView(R.id.sbDistance)
    SeekBar sbDistance;


    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_requests_tabs_filter_content;
    }

    @Override
    protected void doOnCreated(View view) {
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.CHANGE_MAP_TAB) {

            Integer pageCode = (Integer) event.getObject();

            if(pageCode == 0) {
                llFilterContent.setBackgroundColor(getResources().getColor(R.color.red_8a1a29));
            } else {
                llFilterContent.setBackgroundColor(getResources().getColor(R.color.purple_522d6d));
            }
            llFilterContent.invalidate();
        }
    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
