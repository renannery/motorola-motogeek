package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.model.Part;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by jsilva on 5/6/15.
 */
public class FragmentPartDetail extends BaseFragment {

    private Part part;

    @InjectView(R.id.ivPartPicture)
    ImageView ivPartPicture;

    @InjectView(R.id.llSelect)
    LinearLayout llSelect;

    @OnClick(R.id.llSelect)
    public void selectPart(){
        ((ActivityWorkOrder)getActivity()).addPart(part);
        getFragmentManager().popBackStack(FragmentQuotationEdit.class.getName(), 0);
    }

    @OnClick(R.id.llOther)
    public void selectOther(){
        getFragmentManager().popBackStack();
    }


    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_part_detail;
    }

    @Override
    protected void doOnCreated(View view) {
        Bundle bundle = getArguments();
        Long partId = bundle.getLong(Const.MOTO_REQUEST_ID);
        Realm realm = Realm.getInstance(getActivity());
        part = realm.where(Part.class).equalTo("id", partId).findFirst();

        String partPicture = part.getPicture();

        Picasso.with(this.getActivity())
                .load(partPicture)
                .centerCrop()
                .fit()
                .into(ivPartPicture);

    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText("WO 123456");
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
