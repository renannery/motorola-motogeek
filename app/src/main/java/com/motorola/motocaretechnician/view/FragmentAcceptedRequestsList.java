package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoAcceptedRequests;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentAcceptedRequestsList extends BaseFragment {
    @InjectView(R.id.rvAcceptedRequests)
    RecyclerView rvAcceptedRequests;

    private RecyclerView.LayoutManager mLayoutManager;
    private AdapterMotoAcceptedRequests mAdapter;

    @Override
    protected int layoutToInflate() {
        return R.layout.tab_accepted_requests;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.REQUEST_SCROLL) {
            MotoRequest motoRequest = (MotoRequest) event.getObject();
            if (motoRequest.getStatus().equals(Const.APPOINTMENT_ACCEPTED)) {
                for (int i = 0; i < mAdapter.getItemCount(); i++) {
                    MotoRequest motoRequestTarget = mAdapter.getItems().get(i);
                    if (motoRequestTarget.getId() == motoRequest.getId()){
                        rvAcceptedRequests.smoothScrollToPosition(i);
                        break;
                    }
                }
            }
        }

        if(event.getKey() == GenericBus.REQUEST_LIST_REFRESH) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void doOnCreated(View view) {
        rvAcceptedRequests.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvAcceptedRequests.setLayoutManager(mLayoutManager);

        Realm realm = Realm.getInstance(getActivity());
        RealmResults<MotoRequest> motoRequests = realm.where(MotoRequest.class).equalTo(Const.APPOINTMENT_STATUS_FIELD, Const.APPOINTMENT_ACCEPTED).isNull("workOrder").findAll();
        mAdapter = new AdapterMotoAcceptedRequests(getActivity(), motoRequests);
        rvAcceptedRequests.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                MotoRequest motoRequest = (MotoRequest) object;

                Bundle bundle = new Bundle();
                bundle.putInt(Const.REQUEST_DETAIL_FRAGMENT_TO_INFLATE, Const.TYPE_ACCEPTED_REQUEST_LIST);
                bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());

                startActivity(CallManager.requestDetails(getActivity()).putExtras(bundle));
            }
        });
    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
