package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapsInitializer;
import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterNotification;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.ServerCentral;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Notification;
import com.motorola.motocaretechnician.model.User;
import com.motorola.motocaretechnician.network.JsonCentral;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class ActivityMain extends BaseActivity {

    @InjectView(R.id.ivMainUserPhoto)
    ImageView ivMainUserPhoto;

    @InjectView(R.id.tvMainUserCompleteName)
    TextView tvMainUserCompleteName;

    @InjectView(R.id.llHome)
    LinearLayout llHome;

    @InjectView(R.id.llPendent)
    LinearLayout llPendent;

    @InjectView(R.id.llCurrentRequest)
    LinearLayout llCurrentRequest;

    @InjectView(R.id.llFinishedRequest)
    LinearLayout llFinishedRequest;

    @InjectView(R.id.llChat)
    LinearLayout llChat;

    @InjectView(R.id.llLogout)
    LinearLayout llLogout;

    @InjectView(R.id.llProfile)
    LinearLayout llProfile;

    @InjectView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    private AdapterNotification mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Notification> notifications;

    @OnClick(R.id.llProfile)
    public void onProfileClick() {
        startActivity(CallManager.profile(this));
    }

    @OnClick(R.id.llHome)
    public void onHomeClick() {
        resetToHomeFragment();
        closeDrawer();
    }

    @OnClick(R.id.llPendent)
    public void onPendentClick() {
        setFragmentMain(CallManager.fragmentRequestsMap(), true);
        EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, 0, null));
        closeDrawer();
    }

    @OnClick(R.id.llCurrentRequest)
    public void onCurrentRequestClick() {
        setFragmentMain(CallManager.fragmentOpenedServices(), true);
        closeDrawer();
    }

    @OnClick(R.id.llFinishedRequest)
    public void onFinishedRequestClick() {
        setFragmentMain(CallManager.fragmentPastServices(), true);
        closeDrawer();
    }

    @OnClick(R.id.llChat)
    public void onChatClick() {
        setFragmentMain(CallManager.fragmentChatLobby(), true);
        closeDrawer();
    }

    @OnClick(R.id.llLogout)
    public void onLogoutClick() {
        logout();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_main;
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.NOTIFICATION_UPDATE) {
            if (event.getResult().isSuccess()) {
                notifications = event.getObjects();

                Realm realm = Realm.getInstance(this);
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(notifications);
                realm.commitTransaction();
                realm.close();
                mAdapter = new AdapterNotification(this, notifications);

                mAdapter.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onClickItem(View view, Object object) {
                        Notification notification = (Notification) object;
                        Toast.makeText(view.getContext(), notification.getId(), Toast.LENGTH_SHORT).show();
                    }
                });
                rvNotifications.setAdapter(mAdapter);
            } else {
                Toast.makeText(this, event.getResult().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void doOnFirstTime() {
        setFragmentMain(CallManager.fragmentDashboard(), false);
    }

    @Override
    public void doOnCreated(Bundle savedInstanceState) {
        MapsInitializer.initialize(getApplicationContext());

        notifications = new ArrayList<>();
        rvNotifications.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvNotifications.setLayoutManager(mLayoutManager);

        mAdapter = new AdapterNotification(this, notifications);
        rvNotifications.setAdapter(mAdapter);
        User user = UserUtils.getInstance().getLoggedUser(this);
        JsonCentral.getInstance().returnNotifications(String.valueOf(user.getId()));

        //FIXME some devices return user object as null, but was debugged and we didn't see any problems after checkout on the last master commit branch
        Picasso.with(this)
                .load(ServerCentral.getInstance().getImageWithPath(this, user.getPhoto()))
                .fit()
                .centerCrop()
                .into(ivMainUserPhoto);

        tvMainUserCompleteName.setText(user.getFirstName().concat(" ").concat(user.getLastName()));
    }

    private void redirectToLogin() {
        startActivity(CallManager.welcome(this));
        finish();
    }

    private void logout() {
        UserUtils.getInstance().setLogoff(this);
        startActivity(CallManager.welcome(this));
        finish();
    }

    @Override
    protected boolean hasDrawer() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
