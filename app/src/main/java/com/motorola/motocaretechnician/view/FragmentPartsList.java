package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoParts;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryPart;
import com.motorola.motocaretechnician.model.Part;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import io.realm.Realm;

/**
 * Created by jsilva on 5/6/15.
 */
public class FragmentPartsList extends BaseFragment {

    @InjectView(R.id.rvParts)
    RecyclerView rvParts;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoParts mAdapter;
    private List<String> partsIssues = new ArrayList<String>();


    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_parts_list;
    }

    @Override
    protected void doOnCreated(View view) {

        rvParts.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvParts.setLayoutManager(mLayoutManager);

        Realm realm = Realm.getInstance(getActivity());

        Bundle bundle = getArguments();
        Long categoryId = bundle.getLong(Const.MOTO_REQUEST_ID);

        CategoryPart categoryPart = realm.where(CategoryPart.class).equalTo("code", categoryId).findFirst();

        mAdapter = new AdapterMotoParts(getActivity(), categoryPart.getParts());
        rvParts.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                Part part = (Part) object;
               ((ActivityWorkOrder)getActivity()).addPart(part);

                getFragmentManager().popBackStack(FragmentQuotationEdit.class.getName(), 0);

            }
        });


    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText("WO 123456");
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
