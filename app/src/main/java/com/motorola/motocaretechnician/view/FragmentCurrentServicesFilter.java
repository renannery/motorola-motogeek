package com.motorola.motocaretechnician.view;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.model.Device;

import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentCurrentServicesFilter extends BaseFragment {

    @InjectView(R.id.spinnerDevices)
    Spinner spinnerDevices;

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_current_services_filter_content;
    }

    @Override
    protected void doOnCreated(View view) {

        Realm realm = Realm.getInstance(getActivity());

        RealmResults<Device> devices = realm.allObjectsSorted(Device.class, Const.DEVICE_COLUMN_MODEL,true);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getActivity(), R.layout.spinner_item);
        for(int i = 0; i< devices.size();i++){
            arrayAdapter.add(devices.get(i).getModel());
        }
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerDevices.setAdapter(arrayAdapter);

    }

    @Override
    protected void setUpToolbar(View view) {

    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
