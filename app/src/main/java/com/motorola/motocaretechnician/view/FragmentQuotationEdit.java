package com.motorola.motocaretechnician.view;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoIssuesSelected;
import com.motorola.motocaretechnician.control.AdapterMotoPartsSelected;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.Issue;
import com.motorola.motocaretechnician.model.Part;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by jsilva on 5/4/15.
 */
public class FragmentQuotationEdit extends BaseFragment {

    @InjectView(R.id.llIssues)
    LinearLayout llIssues;

    @InjectView(R.id.rvIssues)
    RecyclerView rvIssues;

    @InjectView(R.id.rvParts)
    RecyclerView rvParts;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoIssuesSelected mIssuesAdapter;

    private AdapterMotoPartsSelected mPartsAdapter;

    @OnClick(R.id.llIssues)
    public void onIssuesClick() {
        ((ActivityWorkOrder)getActivity()).setFragmentPartsIssuesList(CallManager.fragmentIssuesCategoriesList(), true);
    }

    @OnClick(R.id.llParts)
    public void onPartsClick() {
        ((ActivityWorkOrder)getActivity()).setFragmentPartsIssuesList(CallManager.fragmentPartsCategoriesList(), true);
    }

    @OnClick(R.id.llAcceptQuotation)
    public void onAcceptClick(){
        EventBus.getDefault().post(new GenericBus(GenericBus.BUTTON_CREATE_QUOTATION, ((ActivityWorkOrder) getActivity()).parts, null));
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.llCancelQuotation)
    public void onCancelClick(){
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_quotation_edit;
    }

    @Override
    protected void doOnCreated(View view) {

        rvIssues.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvIssues.setLayoutManager(mLayoutManager);

        mIssuesAdapter = new AdapterMotoIssuesSelected(getActivity(), ((ActivityWorkOrder)getActivity()).issues);
        rvIssues.setAdapter(mIssuesAdapter);

        mIssuesAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                Issue issue = (Issue) object;
                ((ActivityWorkOrder) getActivity()).addIssue(issue);

            }
        });


        rvParts.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvParts.setLayoutManager(mLayoutManager);

        mPartsAdapter = new AdapterMotoPartsSelected(getActivity(), ((ActivityWorkOrder)getActivity()).parts);
        rvParts.setAdapter(mPartsAdapter);

        mPartsAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClickItem(View view, Object object) {
                Part part = (Part) object;
                ((ActivityWorkOrder) getActivity()).addPart(part);
                getFragmentManager().popBackStack(FragmentQuotationEdit.class.getName(), 0);

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

       resizeIssuesPartsRecycleView();


    }

    public void resizeIssuesPartsRecycleView(){

        int viewIssuesHeight = ((ActivityWorkOrder)getActivity()).issues.size() * (getResources().getDimensionPixelSize(R.dimen.button_height) + getResources().getDimensionPixelSize(R.dimen.margin_bottom_selected_issue_part));
        rvIssues.getLayoutParams().height = viewIssuesHeight;
        mIssuesAdapter.notifyDataSetChanged();

        int viewPartsHeight = ((ActivityWorkOrder)getActivity()).parts.size() * (getResources().getDimensionPixelSize(R.dimen.button_height) + getResources().getDimensionPixelSize(R.dimen.margin_bottom_selected_issue_part));
        rvParts.getLayoutParams().height = viewPartsHeight;
        mPartsAdapter.notifyDataSetChanged();
    }


    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText("WO 123456");
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.REMOVE_PARTS_OR_ISSUES) {
           resizeIssuesPartsRecycleView();
        }
    }
}
