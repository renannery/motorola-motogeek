package com.motorola.motocaretechnician.view;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.DateUtils;
import com.motorola.motocaretechnician.control.EditTextValidator;
import com.motorola.motocaretechnician.model.Appointment;
import com.motorola.motocaretechnician.model.Location;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.Scheduling;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by jsilva on 4/8/15.
 */
public class FragmentWorkOrderAppointment extends BaseFragment  {

    @InjectView(R.id.address)
    MaterialEditText tvAddress;

    @InjectView(R.id.district)
    MaterialEditText tvDistrict;

    @InjectView(R.id.city)
    MaterialEditText tvCity;

    @InjectView(R.id.state)
    MaterialEditText tvState;

    @InjectView(R.id.postalCode)
    MaterialEditText tvPostalCode;

    @InjectView(R.id.referencePoint)
    MaterialEditText tvReferencePoint;

    @InjectView(R.id.hour)
    MaterialEditText tvHour;

    @InjectView(R.id.date)
    MaterialEditText tvDate;

    @InjectView(R.id.llSubmit)
    LinearLayout llSubmit;

    private MotoRequest motoRequest;

    private Appointment appointment;

    @OnClick(R.id.llSubmit)
    void onBtSubmitClick(){
        if (tvDate.validate() && tvHour.validate()) {
            Realm realm = Realm.getInstance(getActivity());
            realm.beginTransaction();

            Scheduling scheduling = realm.createObject(Scheduling.class);

            Location location = realm.createObject(Location.class);
            location.setCity(tvCity.getText().toString());
            location.setStreet(tvAddress.getText().toString());
            location.setDistrict(tvDistrict.getText().toString());
            location.setState(tvState.getText().toString());
            location.setZip(tvPostalCode.getText().toString());
            location.setReferencePoint(tvReferencePoint.getText().toString());

            scheduling.setLocation(location);
            scheduling.setDateTime(DateUtils.getddMMyyyyToIso8601(tvDate.getText().toString() + " " + tvHour.getText().toString()));

            appointment.setScheduling(scheduling);
            appointment = realm.copyToRealmOrUpdate(appointment);
            motoRequest.setAppointment(appointment);
            realm.commitTransaction();
            realm.close();
            Toast.makeText(getActivity(), "Appointment saved", Toast.LENGTH_LONG).show();
            getFragmentManager().popBackStack();
        }


    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_work_order_appointment;
    }

    @Override
    protected void doOnCreated(View view) {

        Bundle b = getArguments();
        long motoRequestId = b.getLong(Const.MOTO_REQUEST_ID);

        Realm realm = Realm.getInstance(getActivity());
        motoRequest = realm.where(MotoRequest.class).equalTo(Const.MOTO_REQUEST_ID, motoRequestId).findFirst();
        if(motoRequest != null) {
            appointment = motoRequest.getAppointment();
            if (appointment != null) {
                fillFields();
            } else {
                appointment = new Appointment();
            }
            realm.close();
        }
        tvDate.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));
        tvHour.addValidator(new EditTextValidator(getResources().getString(R.string.login_edit_text_message_error)));


        tvDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    if (!tvDate.getText().toString().isEmpty()) {
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            mcurrentTime.setTime(sdf.parse(tvDate.getText().toString()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
                    int month = mcurrentTime.get(Calendar.MONTH);
                    int year = mcurrentTime.get(Calendar.YEAR);
                    DatePickerDialog mDatePickerStart;

                    mDatePickerStart = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            if (datePicker.isShown()) {
                                String startTime = tvDate.getText().toString();

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.DAY_OF_MONTH, day);
                                calendar.set(Calendar.MONTH, month);
                                calendar.set(Calendar.YEAR, year);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                dateFormat.format(calendar.getTime());

                                String strHrsToShow = dateFormat.format(calendar.getTime());

                                tvDate.setText(strHrsToShow);
                            }
                        }
                    }, year, month, day);
                    mDatePickerStart.setTitle("Select Hour");
                    mDatePickerStart.show();
                }
            }
        });

        tvHour.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    if (!tvHour.getText().toString().isEmpty()) {
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                            mcurrentTime.setTime(sdf.parse(tvHour.getText().toString()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePickerStart;
                    final TimePickerDialog mTimePicker;

                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            if (timePicker.isShown()) {
                                String startTime = tvHour.getText().toString();

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                                calendar.set(Calendar.MINUTE, selectedMinute);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                                dateFormat.format(calendar.getTime());

                                String strHrsToShow = dateFormat.format(calendar.getTime());

                                tvHour.setText(strHrsToShow);
                            }
                        }
                    }, hour, minute, false);
                    mTimePicker.setTitle("Select Hour");
                    mTimePicker.show();
                }
            }
        });

    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(getActivity().getString(R.string.pending_request_details_toolbar_title));
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }


    public void fillFields() {
        tvCity.setText(appointment.getScheduling().getLocation().getCity());
        tvAddress.setText(appointment.getScheduling().getLocation().getStreet());
        tvState.setText(appointment.getScheduling().getLocation().getState());
        tvDistrict.setText(appointment.getScheduling().getLocation().getDistrict());
        tvPostalCode.setText(appointment.getScheduling().getLocation().getZip());
        tvReferencePoint.setText(appointment.getScheduling().getLocation().getReferencePoint());

        if(!appointment.getScheduling().getDateTime().isEmpty()){
            tvDate.setText(DateUtils.getIso8601ddMMyyyy(appointment.getScheduling().getDateTime()));
            tvHour.setText(DateUtils.getIso8601hhmm(appointment.getScheduling().getDateTime()));
        }

    }

}
