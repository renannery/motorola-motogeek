package com.motorola.motocaretechnician.view;

import android.animation.ObjectAnimator;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.model.GenericBus;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by jsilva on 4/17/15.
 */
public class FragmentRequestsTabs extends BaseFragment {

    private ActionBarDrawerToggle drawerToggle;
    private SlidingUpPanelLayout slidingUpPanelLayout;

    @InjectView(R.id.ivSetaFiltro)
    ImageView ivSetaFiltro;

    @InjectView(R.id.llSliderFilter)
    LinearLayout llSliderFilter;



    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_requests_tabs;
    }

    @Override
    protected void doOnCreated(View view) {

        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        slidingUpPanelLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);

        slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelExpanded(View panel) {
                ObjectAnimator.ofFloat(ivSetaFiltro, View.ROTATION, 0f, 180f).setDuration(150).start();
            }

            @Override
            public void onPanelCollapsed(View panel) {
                ObjectAnimator.ofFloat(ivSetaFiltro, View.ROTATION, 180f, 0f).setDuration(150).start();
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });

    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.CHANGE_MAP_TAB) {

            Integer pageCode = (Integer) event.getObject();

            if(pageCode == 0){
                llSliderFilter.setBackgroundColor(getResources().getColor(R.color.red_8a1a29));
            }else{
                llSliderFilter.setBackgroundColor(getResources().getColor(R.color.purple_522d6d));
            }
            llSliderFilter.invalidate();
        }
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        setHasOptionsMenu(true);
        tvTitleToolbar.setText(getActivity().getString(R.string.request_map_toolbar_title));


        drawerToggle = new ActionBarDrawerToggle(getActivity(), ((ActivityMain) getActivity()).getDrawer(), toolbar, R.string.send, R.string.send);
        ((ActivityMain) getActivity()).setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
