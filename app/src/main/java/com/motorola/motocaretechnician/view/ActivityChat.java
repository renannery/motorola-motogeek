package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterChat;
import com.motorola.motocaretechnician.control.Config;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.DateUtils;
import com.motorola.motocaretechnician.control.ServerCentral;
import com.motorola.motocaretechnician.control.SessionManager;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.control.XMPPUtils;
import com.motorola.motocaretechnician.model.ChatMessage;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.User;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class ActivityChat extends BaseActivity  {

    @InjectView(R.id.rvChat)
    RecyclerView rvChat;

    @InjectView(R.id.etChatInput)
    EditText etChatInput;

    @InjectView(R.id.ibSend)
    ImageButton ibSend;

    @InjectView(R.id.ibSendPicture)
    ImageButton ibSendPicture;

    @InjectView(R.id.ivUser)
    ImageView ivUser;

    @InjectView(R.id.tvUsername)
    TextView tvUsername;

    private AdapterChat mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ChatMessage> messages = new ArrayList<>();
    private String username;
    private User user;

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void doOnFirstTime() {

    }

    @Override
    protected void doOnCreated(Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle b = getIntent().getExtras();
        username = b.getString(Const.USER_USERNAME);
        tvTitleToolbar.setText(getString(R.string.chat_toolbar_title));

        rvChat.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(this);
        rvChat.setLayoutManager(mLayoutManager);

        mAdapter = new AdapterChat(this, messages);
        rvChat.setAdapter(mAdapter);

        etChatInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ibSendPicture.setVisibility(View.GONE);
                    ibSend.setVisibility(View.VISIBLE);
                } else {
                    ibSendPicture.setVisibility(View.VISIBLE);
                    ibSend.setVisibility(View.GONE);
                }
            }
        });

        loadChatHistory();
        setupUserDetails();
    }

    private void setupUserDetails() {
        Picasso.with(this)
                .load(ServerCentral.getInstance().getImageWithPath(this, user.getPhoto()))
                .centerCrop()
                .fit()
                .into(ivUser);

        tvUsername.setText(user.getFirstName());
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_chat;
    }

    @OnClick(R.id.ibSend)
    public void onIbSendMessageClick() {

        try {

            Message message = new Message();
            message.setBody(etChatInput.getText().toString());
            message.setFrom(SessionManager.getInstance().getCurrentUser().getUsername().concat("@").concat(Config.XMPP_HOST).concat("/Smack"));
            message.setTo(username);
            ChatMessage chatMessage = new ChatMessage(message);
            mAdapter.addItem(chatMessage);
            XMPPUtils.getInstance(this).sendMessage(message);

            rvChat.post(new Runnable() {
                @Override
                public void run() {
                    etChatInput.setText("");
                    mAdapter.notifyDataSetChanged();
                    if (mAdapter.getItemCount() > 0) {
                        rvChat.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                    }
                }
            });
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void onIbSendFileClick() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageFrom(SessionManager.getInstance().getCurrentUser().getUsername());
        chatMessage.setMessage("Sent file: icon_test.png");
        chatMessage.setDateTime(DateUtils.currentTime());
        chatMessage.setMessageTo(username);
        mAdapter.addItem(chatMessage);
        XMPPUtils.getInstance(this).sendFile(username.concat("@").concat(Config.XMPP_HOST).concat("/SAP2ACWARNERY01"));

        rvChat.post(new Runnable() {
            @Override
            public void run() {
                etChatInput.setText("");
                mAdapter.notifyDataSetChanged();
                if (mAdapter.getItemCount() > 0) {
                    rvChat.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                }
            }
        });
    }

    public void loadChatHistory(){
        Realm realm = Realm.getInstance(this);
        RealmResults<ChatMessage> chatMessages = realm.where(ChatMessage.class)
                .beginGroup()
                    .equalTo(ChatMessage.MESSAGE_FROM, SessionManager.getInstance().getCurrentUser().getUsername())
                    .equalTo(ChatMessage.MESSAGE_TO, username)
                .endGroup()
                .or()
                .beginGroup()
                    .equalTo(ChatMessage.MESSAGE_FROM, username)
                    .equalTo(ChatMessage.MESSAGE_TO, SessionManager.getInstance().getCurrentUser().getUsername())
                .endGroup()
                .findAllSorted(ChatMessage.DATE_TIME);

        mAdapter.addItems(chatMessages);
        if (mAdapter.getItemCount() > 0) {
            rvChat.smoothScrollToPosition(mAdapter.getItemCount() - 1);
        }

        realm.beginTransaction();
        for(int i = 0; i < chatMessages.size(); i++) {
            chatMessages.get(i).setIsDisplay(true);
        }
        realm.commitTransaction();
        user = realm.where(User.class).equalTo(Const.USER_USERNAME, username).findFirst();
        realm.close();
    }

    @Override
    protected boolean hasDrawer() {
        return false;
    }

    public void onEventMainThread(GenericBus event) {
        Message message;

        if(event.getKey() == GenericBus.CHAT_MESSAGE_BODY) {
            ChatMessage chatMessage = (ChatMessage) event.getObject();

            if(UserUtils.getInstance().clearUsername(chatMessage.getMessageFrom()).equals(username)) {
                Realm realm = Realm.getInstance(this);
                RealmResults<ChatMessage> chatMessages = realm.where(ChatMessage.class)
                        .equalTo(ChatMessage.MESSAGE_FROM, chatMessage.getMessageFrom())
                        .equalTo(ChatMessage.IS_DISPLAY, false)
                        .findAllSorted(ChatMessage.DATE_TIME);

                mAdapter.addItems(chatMessages);

                realm.beginTransaction();
                for(int i = 0; i < chatMessages.size(); i++) {
                    chatMessages.get(i).setIsDisplay(true);
                }
                realm.commitTransaction();
                realm.close();

                rvChat.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                        if (mAdapter.getItemCount() > 0) {
                            rvChat.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    }
                });
                return;
            }
            return;
        }

        if (event.getKey() == GenericBus.CHAT_MESSAGE_COMPOSING) {
            message = (Message) event.getObject();
            Log.d("ChatMessage", message.toString());
            return;
        }

        if (event.getKey() == GenericBus.CHAT_MESSAGE_PAUSED) {
            message = (Message) event.getObject();
            Log.d("CHAT_MESSAGE_PAUSED", message.toString());
            return;
        }

        if (event.getKey() == GenericBus.CHAT_MESSAGE_ACTIVE) {
            message = (Message) event.getObject();
            Log.d("CHAT_MESSAGE_ACTIVE", message.toString());
            return;
        }

        if (event.getKey() == GenericBus.CHAT_MESSAGE_DISPLAYED) {
            message = (Message) event.getObject();
            Log.d("CHAT_MESSAGE_DISPLAYED", message.toString());
            return;
        }

        if (event.getKey() == GenericBus.CHAT_MESSAGE_RECEIVED) {
            message = (Message) event.getObject();
            Log.d("CHAT_MESSAGE_RECEIVED", message.toString());
            return;
        }

        if (event.getKey() == GenericBus.CHAT_FILE_RECEIVED) {
            ChatMessage chatMessage = (ChatMessage) event.getObject();
            mAdapter.addItem(chatMessage);
            if (mAdapter.getItemCount() > 0) {
                rvChat.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }
            return;
        }
    }
}