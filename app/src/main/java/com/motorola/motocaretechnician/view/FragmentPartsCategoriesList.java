package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AdapterMotoPartsCategories;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.interfaces.OnItemClickListener;
import com.motorola.motocaretechnician.model.CategoryPart;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.network.JsonCentral;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

/**
 * Created by jsilva on 5/4/15.
 */
public class FragmentPartsCategoriesList extends BaseFragment {

    @InjectView(R.id.rvParts)
    RecyclerView rvParts;

    @InjectView(R.id.progressbar)
    ProgressBar progressbar;

    private RecyclerView.LayoutManager mLayoutManager;

    private AdapterMotoPartsCategories mAdapter;
    private List<CategoryPart> categoryParts = new ArrayList<CategoryPart>();

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_parts_categories_list;
    }

    @Override
    protected void doOnCreated(View view) {

        rvParts.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvParts.setLayoutManager(mLayoutManager);

        JsonCentral.getInstance().partsCategories();

    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText("WO 123456");
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.PARTS_CATEGORIES_LIST) {
            if (event.getResult().isSuccess()) {
                ArrayList<CategoryPart> categoriesItems = event.getObjects();
                if (categoriesItems != null) {
                    for(CategoryPart category : categoriesItems){
                        Realm realm = Realm.getInstance(getActivity());
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(category);

                        categoryParts.add(category);

                        realm.commitTransaction();
                    }
                    progressbar.setVisibility(View.GONE);
                    mAdapter = new AdapterMotoPartsCategories(getActivity(), categoryParts);
                    rvParts.setAdapter(mAdapter);

                    mAdapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onClickItem(View view, Object object) {
                            CategoryPart issue = (CategoryPart) object;

                            Bundle bundle = new Bundle();
                            bundle.putLong(Const.MOTO_REQUEST_ID, issue.getCode());

                            Fragment fragment = CallManager.fragmentPartsList();
                            fragment.setArguments(bundle);

                            ((ActivityWorkOrder)getActivity()).setFragmentParts(fragment, true);

                        }
                    });

                }
            }
        }
    }
}
