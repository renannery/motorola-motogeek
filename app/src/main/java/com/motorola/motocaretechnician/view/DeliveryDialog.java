package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.motorola.motocaretechnician.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by jsilva on 6/11/15.
 */
public class DeliveryDialog extends DialogFragment {

    private CustomDialogButtonsCallback buttonsCallback;

    @InjectView(R.id.cpfDigits)
    public EditText cpfDigits;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick(R.id.buttonOk)
    public void onOkClick() {
        if(cpfDigits.getText().length() == 3) {
            buttonsCallback.doOkConfirmClick();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shape);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_delivery_alert, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    public void setCustomDialogCallback(CustomDialogButtonsCallback callback) {
        this.buttonsCallback = callback;
    }

    public interface CustomDialogButtonsCallback {
        void doOkConfirmClick();
    }
}
