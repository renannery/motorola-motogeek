package com.motorola.motocaretechnician.view;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.AnimationUtils;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.model.Appointment;
import com.motorola.motocaretechnician.model.Dashboard;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.network.JsonCentral;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentDashboard extends BaseFragment {
    @InjectView(R.id.rlAcceptedRequests)
    RelativeLayout rlAcceptedRequests;

    @InjectView(R.id.rlPendingRequests)
    RelativeLayout rlPendingRequests;

    @InjectView(R.id.rlOpenedService)
    RelativeLayout rlOpenedService;

    @InjectView(R.id.rlChat)
    RelativeLayout rlChat;

    @InjectView(R.id.rlChatBalloon)
    RelativeLayout rlChatBalloon;

    @InjectView(R.id.rlSeeNotifications)
    RelativeLayout rlSeeNotifications;

    @InjectView(R.id.tvNotificationCount)
    TextView tvNotificationCount;

    @InjectView(R.id.tvCountAccepted)
    TextView tvCountAccepted;

    @InjectView(R.id.tvCountPending)
    TextView tvCountPending;

    @InjectView(R.id.tvCountServices)
    TextView tvCountServices;

    @InjectView(R.id.tvCountChat)
    TextView tvCountChat;

    @InjectView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectView(R.id.ivRightDevice)
    ImageView ivRightDevice;

    @InjectView(R.id.ivLeftDevice)
    ImageView ivLeftDevice;

    @InjectView(R.id.ivIconAccepted)
    ImageView ivIcon;

    @InjectView(R.id.progressbar)
    ProgressBar progressbar;

    private ActionBarDrawerToggle drawerToggle;
    private ArrayList<View> views;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public int layoutToInflate() {
        return R.layout.new_fragment_dashboard;
    }

    @Override
    protected void doOnCreated(View view) {

        JsonCentral.getInstance().dashboard(String.valueOf(UserUtils.getInstance().getLoggedUser(getActivity()).getId()));
        JsonCentral.getInstance().returnRequests(String.valueOf(UserUtils.getInstance().getLoggedUser(getActivity()).getId()));

        views = new ArrayList<>(4);
        views.add(rlChat);
        views.add(rlAcceptedRequests);
        views.add(rlPendingRequests);
        views.add(rlOpenedService);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
    }

    @OnClick(R.id.rlChat)
    public void onChatClick() {
        ((ActivityMain) getActivity()).setFragmentMain(CallManager.fragmentChatLobby(), true);
    }

    @OnClick(R.id.rlAcceptedRequests)
    public void onAcceptedRequests() {
        ((ActivityMain) getActivity()).setFragmentMain(CallManager.fragmentRequestsMap(), true);
        EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, 1, null));
    }

    @OnClick(R.id.rlPendingRequests)
    public void onPendingRequests() {
        ((ActivityMain) getActivity()).setFragmentMain(CallManager.fragmentRequestsMap(), true);
        EventBus.getDefault().post(new GenericBus(GenericBus.CHANGE_MAP_TAB, 0, null));
    }

    @OnClick(R.id.rlOpenedService)
    public void onOpenedService() {
        ((ActivityMain) getActivity()).setFragmentMain(CallManager.fragmentOpenedServices(), true);
    }

    @OnClick(R.id.rlSeeNotifications)
    public void onSeeNotificationsClick() {
        ((ActivityMain) getActivity()).openRightDrawer();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ((ActivityMain) getActivity()).getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActivityMain) getActivity()).getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.RIGHT);
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        tvTitleToolbar.setText(getActivity().getString(R.string.dashboard_toolbar_title));

        drawerToggle = new ActionBarDrawerToggle(getActivity(), ((ActivityMain) getActivity()).getDrawer(), toolbar, R.string.send, R.string.send);
        ((ActivityMain) getActivity()).setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void refreshItems() {
        JsonCentral.getInstance().dashboard(String.valueOf(UserUtils.getInstance().getLoggedUser(getActivity()).getId()));
    }

    private void onItemsLoadComplete() {
        progressbar.setVisibility(View.GONE);
        if(!mSwipeRefreshLayout.isRefreshing()) {
            for (int i = 0; i < 4; i++) {
                AnimationUtils.slideUpFromBottom(views.get(i), getActivity(), (i + 1) * 200);
            }
            setScreenBuildAnimation();
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(GenericBus event) {
        if (event.getKey() == GenericBus.USER_DASHBOARD) {
            if (event.getResult().isSuccess()) {
                ArrayList<Dashboard> dashboardItems =  event.getObjects();
                if (dashboardItems != null) {
                    Dashboard chatDashboardItem = new Dashboard();
                    chatDashboardItem.setId(0);
                    chatDashboardItem.setCount(3);
                    chatDashboardItem.setChatItem(true);
                    chatDashboardItem.setDescription(getActivity().getString(R.string.dashboard_my_chats));
                    dashboardItems.add(chatDashboardItem);

                    Realm realm = Realm.getInstance(getActivity());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(dashboardItems);
                    realm.commitTransaction();
                    RealmResults<Dashboard> dashboardList = realm.where(Dashboard.class).findAllSorted("id");
                    realm.close();

                    //TODO o webservice devera ser alterado para nao enviar items, porem nao mudamos ainda pois o app sera apresentado
                    for(int i = 0;i < views.size(); i++) {
                        switch (i) {
                            case 0:
                                tvCountChat.setText(String.valueOf(dashboardList.get(i).getCount()));
                                break;
                            case 1:
                                tvCountAccepted.setText(String.valueOf(dashboardList.get(i).getCount()));
                                break;
                            case 2:
                                tvCountPending.setText(String.valueOf(dashboardList.get(i).getCount()));
                                break;
                            case 3:
                                tvCountServices.setText(String.valueOf(dashboardList.get(i).getCount()));
                                break;
                        }
                    }

                    onItemsLoadComplete();
                }
            } else {
                Toast.makeText(getActivity(), event.getResult().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        if (event.getKey() == GenericBus.NOTIFICATION_UPDATE) {
            if (event.getResult().isSuccess()) {
                tvNotificationCount.setText(String.valueOf(event.getObjects().size()));
            }
        }

        if (event.getKey() == GenericBus.USER_NEW_REQUESTS) {
            if (event.getResult().isSuccess()) {
                ArrayList<MotoRequest> motoRequests =  event.getObjects();
                if (motoRequests != null) {

//                    for (final MotoRequest request: motoRequests) {
//
//                        new MaterialDialog.Builder(getActivity())
//                                .title("New Request")
//                                .customView(R.layout.dialog_new_request, true)
//                                .showListener(new DialogInterface.OnShowListener() {
//                                    @Override
//                                    public void onShow(DialogInterface dialog) {
//                                        Dialog dialogInterface = (Dialog) dialog;
//                                        Context context = dialogInterface.getContext();
//
//                                        TextView tvUserName = (TextView) dialogInterface.findViewById(R.id.tvUserName);
//                                        ImageView ivPhoto = (ImageView) dialogInterface.findViewById(R.id.ivPhoto);
//                                        TextView tvDate = (TextView) dialogInterface.findViewById(R.id.tvDate);
//                                        TextView tvIssue = (TextView) dialogInterface.findViewById(R.id.tvIssue);
//                                        TextView tvAddress = (TextView) dialogInterface.findViewById(R.id.tvAddress);
//                                        TextView tvCityState = (TextView) dialogInterface.findViewById(R.id.tvCityState);
//
//                                        tvUserName.setText(request.getUser().getCompleteName());
//                                        tvDate.setText(request.getDate());
//                                        tvIssue.setText(request.getIssue());
//                                        tvCityState.setText(request.getSuggestedCityState());
//                                        tvAddress.setText(request.getSuggestedAddress());
//
//                                        Picasso.with(context)
//                                                .load(ServerCentral.getInstance().getImageWithPath(context, request.getUser().getPhoto()))
//                                                .fit()
//                                                .centerCrop()
//                                                .into(ivPhoto);
//                                    }
//                                })
//                                .callback(new MaterialDialog.ButtonCallback() {
//                                    @Override
//                                    public void onPositive(MaterialDialog dialog) {
//                                        Toast.makeText(getActivity(), request.getId() + " " + request.getUser().getCompleteName(), Toast.LENGTH_SHORT).show();
//                                    }
//
//                                    @Override
//                                    public void onNegative(MaterialDialog dialog) {
//                                        Toast.makeText(getActivity(), request.getId() + " " + request.getUser().getCompleteName() + " Negado", Toast.LENGTH_SHORT).show();
//                                    }
//                                })
//                                .positiveText("ACEITAR")
//                                .negativeText("NEGAR")
//                                .show();
//                    }

                    Realm realm = Realm.getInstance(getActivity());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(motoRequests);
                    realm.commitTransaction();
                    realm.close();
                } else {
                    //TODO change error text

                }
            } else {
                //TODO change error text
                Toast.makeText(getActivity(), event.getResult().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setScreenBuildAnimation() {
        ObjectAnimator animateRightDevice = ObjectAnimator.ofFloat(ivRightDevice, View.TRANSLATION_X, 0, - ivRightDevice.getDrawable().getMinimumWidth());
        ObjectAnimator animateLeftDevice = ObjectAnimator.ofFloat(ivLeftDevice, View.TRANSLATION_X, 0, ivLeftDevice.getDrawable().getMinimumWidth());

        ObjectAnimator iconScaleUpX = ObjectAnimator.ofFloat(rlChatBalloon, View.SCALE_X, 0.5f, 1);
        ObjectAnimator iconScaleUpY = ObjectAnimator.ofFloat(rlChatBalloon, View.SCALE_Y, 0.5f, 1);

        ObjectAnimator fadeInChat = ObjectAnimator.ofFloat(rlChatBalloon, View.ALPHA, 0, 1);

        AnimatorSet devicesAnimationSet = new AnimatorSet();
        AnimatorSet chatAnimationSet = new AnimatorSet();

        devicesAnimationSet.play(animateLeftDevice).with(animateRightDevice).after(1000);
        devicesAnimationSet.setDuration(500);
        chatAnimationSet.play(iconScaleUpX)
                .with(iconScaleUpY)
                .with(fadeInChat)
                .after(devicesAnimationSet);
        chatAnimationSet.start();

        int dpValue = 20; // margin in dips
        float d = Utils.getInstance().getDisplayMetrics(getActivity()).density;
        int margin = (int)(dpValue * d);

        int xAxis = ((Utils.getInstance().getDisplayMetrics(getActivity()).widthPixels + margin) / 2) - (ivIcon.getDrawable().getMinimumWidth() / 2);

        ObjectAnimator animateCountAccepted = ObjectAnimator.ofFloat(tvCountAccepted, View.TRANSLATION_X, 0, -xAxis);
        ObjectAnimator animateCountPending = ObjectAnimator.ofFloat(tvCountPending, View.TRANSLATION_X, 0, -xAxis);
        ObjectAnimator animateCountServices = ObjectAnimator.ofFloat(tvCountServices, View.TRANSLATION_X, 0, -xAxis);
        AnimatorSet countAnimatorSet = new AnimatorSet();

        animateCountAccepted.setInterpolator(new DecelerateInterpolator(1.8f));
        animateCountPending.setInterpolator(new DecelerateInterpolator(1.8f));
        animateCountServices.setInterpolator(new DecelerateInterpolator(1.8f));

        animateCountPending.setStartDelay(250);
        animateCountServices.setStartDelay(500);

        countAnimatorSet.play(animateCountAccepted).with(animateCountPending).with(animateCountServices).after(800);
        countAnimatorSet.start();
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
