package com.motorola.motocaretechnician.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by jsilva on 4/28/15.
 */
public class CustomDialog extends DialogFragment {

    private String message;
    private int backgroundColor;
    private int primaryColor;
    private CustomDialogButtonsCallback buttonsCallback;

    @InjectView(R.id.buttonOk)
    Button btOk;

    @InjectView(R.id.buttonCancel)
    Button btCancel;

    @InjectView(R.id.tvMessage)
    TextView tvMessage;

    @InjectView(R.id.llDialog)
    LinearLayout llDialog;

    @OnClick(R.id.buttonOk)
    public void onOkClick() {
        if(buttonsCallback != null) {
            buttonsCallback.doOkConfirmClick();
        }
    }

    @OnClick(R.id.buttonCancel)
    public void onCancelClick() {
        if(buttonsCallback != null) {
            buttonsCallback.doCancelConfirmClick();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            this.message = (String) this.getArguments().get("message");
            this.primaryColor = (int) this.getArguments().get("primaryColor");
            this.backgroundColor = (int) this.getArguments().get("backgroundColor");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_shape);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_custom_alert, container, false);
        ButterKnife.inject(this, view);
        tvMessage.setText(message);
        tvMessage.setTextColor(primaryColor);
        btOk.setBackgroundColor(primaryColor);
        btCancel.setBackgroundColor(primaryColor);
        llDialog.setBackgroundColor(getResources().getColor(R.color.white));
        return view;
    }

    public void setCustomDialogCallback(CustomDialogButtonsCallback callback) {
        this.buttonsCallback = callback;
    }

    public interface CustomDialogButtonsCallback {
        void doOkConfirmClick();
        void doCancelConfirmClick();
    }
}