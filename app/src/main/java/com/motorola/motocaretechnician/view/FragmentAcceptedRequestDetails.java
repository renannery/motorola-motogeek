package com.motorola.motocaretechnician.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.motorola.motocaretechnician.R;
import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.Const;
import com.motorola.motocaretechnician.control.ServerCentral;
import com.motorola.motocaretechnician.control.Utils;
import com.motorola.motocaretechnician.control.XMPPUtils;
import com.motorola.motocaretechnician.model.GenericBus;
import com.motorola.motocaretechnician.model.MotoRequest;
import com.motorola.motocaretechnician.model.User;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import io.realm.Realm;

public class FragmentAcceptedRequestDetails extends BaseFragment {
    @InjectView(R.id.tvUsername)
    TextView tvUsername;

    @InjectView(R.id.tvCityState)
    TextView tvCityState;

    @InjectView(R.id.llCreateWorkOrder)
    LinearLayout llCreateWorkOrder;

    @InjectView(R.id.tvSuggestedAddress)
    TextView tvSuggestedAddress;

    @InjectView(R.id.tvSerialNumber)
    TextView tvSerialNumber;

    @InjectView(R.id.tvDevice)
    TextView tvDevice;

    @InjectView(R.id.tvIssue)
    TextView tvIssue;

    @InjectView(R.id.ivClientPhoto)
    ImageView ivClientPhoto;

    @InjectView(R.id.llCall)
    LinearLayout llCall;

    @InjectView(R.id.ivMap)
    ImageView ivMap;

    @InjectView(R.id.llEditAppointment)
    LinearLayout llEditAppointment;

    private MotoRequest motoRequest;

    @OnClick(R.id.llCall)
    public void onCallClick() {
        startActivity(CallManager.dialNumberToCall(motoRequest.getUser().getPhone()));
    }

    @OnClick(R.id.ivMap)
    public void onMapClick() {
        startActivity(CallManager.map(motoRequest.getAppointment().getScheduling().getLocation().getLatitude(), motoRequest.getAppointment().getScheduling().getLocation().getLongitude()));
    }

    @OnClick(R.id.llCreateWorkOrder)
    public void onCreateWorkOrderClick() {
        Fragment fragment = CallManager.fragmentCreateWorkOrder();
        Bundle args = new Bundle();
        args.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());
        fragment.setArguments(args);
        ((ActivityRequestDetails) getActivity()).setFragmentRequestDetails(fragment, true);
    }

    @OnClick(R.id.llEditAppointment)
    public void onEditAppointmentClick(){
        Bundle bundle = new Bundle();
        bundle.putLong(Const.MOTO_REQUEST_ID, motoRequest.getId());

        Fragment fragment = CallManager.fragmentAcceptedRequestAppointmentEdit();
        fragment.setArguments(bundle);

        ((ActivityRequestDetails) getActivity()).setFragmentAppointmentEdit(fragment, true);
    }

    @OnClick(R.id.llCancelRequest)
    public void onCancelingRequest(){
        final CustomDialog dialog = new CustomDialog();
        dialog.setCustomDialogCallback(new CustomDialog.CustomDialogButtonsCallback() {
            @Override
            public void doOkConfirmClick() {
                Realm realm = Realm.getInstance(getActivity());
                try {
                    realm.beginTransaction();
                    motoRequest.removeFromRealm();
                    realm.commitTransaction();
                    realm.close();
                    EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_LIST_REFRESH, motoRequest, null));
                    getActivity().finish();
                } catch (Exception e) {
                    realm.cancelTransaction();
                    realm.close();
                    EventBus.getDefault().post(new GenericBus(GenericBus.REQUEST_LIST_REFRESH, motoRequest, null));
                }

            }

            @Override
            public void doCancelConfirmClick() {
                dialog.dismiss();
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString("message", getString(R.string.canceling_request_message));
        bundle.putInt("primaryColor", getResources().getColor(R.color.purple_522d6d));
        bundle.putInt("backgroundColor", getResources().getColor(R.color.purple_f3e7fb));
        dialog.setArguments(bundle);


        dialog.show(getFragmentManager(), "dialog");

    }

    @OnClick(R.id.llCustomerNoShow)
    public void onCustomerNoShow(){
        CustomDialog dialog = new CustomDialog();
        Bundle bundle = new Bundle();
        bundle.putString("message",getString(R.string.customer_no_show_message));
        bundle.putInt("primaryColor", getResources().getColor(R.color.purple_522d6d));
        bundle.putInt("backgroundColor", getResources().getColor(R.color.purple_f3e7fb));
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), "dialog");
    }

    @OnClick(R.id.llChat)
    public void onChat(){

        XMPPUtils.getInstance(getActivity()).connect();
        XMPPUtils.getInstance(getActivity()).login();
        Realm realm = Realm.getInstance(getActivity());
        User user = realm.where(User.class).findFirst();


        Bundle bundle = new Bundle();
        bundle.putString(Const.USER_USERNAME, user.getUsername());
        startActivity(CallManager.startChat(getActivity()).putExtras(bundle));
    }

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_accepted_request_details;
    }

    @Override
    protected void doOnCreated(View view) {
        Bundle b = getArguments();
        long appointmentCode = b.getLong(Const.MOTO_REQUEST_ID);

        Realm realm = Realm.getInstance(getActivity());
        motoRequest = realm.where(MotoRequest.class).equalTo(Const.APPOINTMENT_CODE_FIELD, appointmentCode).findFirst();

        if(motoRequest != null) {
            fillFields();
        }
        realm.close();
    }

    @Override
    protected void setUpToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        TextView tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        tvTitleToolbar.setText(getActivity().getString(R.string.request_details_accepted_toolbar_title));
    }

    public void fillFields() {
        tvUsername.setText(String.valueOf(motoRequest.getUser().getFirstName().concat(" ").concat(motoRequest.getUser().getLastName())));
        tvCityState.setText(motoRequest.getUser().getCity());
        //tvDatePeriod.setText(appointment.getScheduling().getDateTime());
        tvSuggestedAddress.setText(motoRequest.getAppointment().getScheduling().getLocation().getStreet());
        tvSerialNumber.setText(motoRequest.getDevice().getSerialNumber());
        tvDevice.setText(motoRequest.getDevice().getModel());
        tvIssue.setText(Utils.getInstance().issuesArrayToString(motoRequest.getIssues()));
        Picasso.with(getActivity())
                .load(ServerCentral.getInstance().getImageWithPath(getActivity(), motoRequest.getUser().getPhoto()))
                .fit()
                .centerCrop()
                .into(ivClientPhoto);
    }

    @Override
    protected boolean isDrawerLocked() {
        return false;
    }
}
