package com.motorola.motocaretechnician;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.motorola.motocaretechnician.control.SessionManager;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.model.User;
import com.motorola.motocaretechnician.view.ActivityMain;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class WorkOrderAddQuotation {
    CountDownLatch countDownLatch = new CountDownLatch(10);

    @Rule
    public ActivityTestRule<ActivityMain> mActivityRule = new ActivityTestRule<>(ActivityMain.class);

    @Before
    public void setUp() {
        User user = UserUtils.getInstance().getLoggedUser(mActivityRule.getActivity());
        SessionManager.getInstance().setCurrentUser(user);
    }

    @Test
    public void addQuotation() throws InterruptedException {
        countDownLatch.await(5, TimeUnit.SECONDS);
        onView(withId(R.id.tvCountServices)).perform(click());

        shouldOpenServiceDetails();
        shouldOpenQuotation();
    }

    public void shouldOpenServiceDetails() throws InterruptedException {
        countDownLatch.await(5, TimeUnit.SECONDS);
        
    }

    public void shouldOpenQuotation() throws InterruptedException {
        onView(withId(R.id.llQuotation)).perform(click());
    }
}
