package com.motorola.motocaretechnician;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.motorola.motocaretechnician.control.CallManager;
import com.motorola.motocaretechnician.control.UserUtils;
import com.motorola.motocaretechnician.view.ActivityWelcome;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginTest {
    public static final String STRING_TO_BE_TYPED = "iamtech@motorola.com";

    @Rule
    public ActivityTestRule<ActivityWelcome> mActivityRule = new ActivityTestRule<>(ActivityWelcome.class);

    @Test
    public void loginAndLogoffTest() {

        if(UserUtils.getInstance().isUserLogged(mActivityRule.getActivity())) {
            logout();
            loginProcedure();
        } else {
            loginProcedure();
        }
    }

    private void loginProcedure() {
        onView(withId(R.id.llStart)).perform(click());

        onView(withId(R.id.llStart)).perform(click());

        onView(withId(R.id.metUsername)).perform(click());

        onView(withId(R.id.metUsername))
                .perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.metPassword))
                .perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard());

        onView(withId(R.id.llLoginButton)).perform(click());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.rlSeeNotifications)).check(matches(ViewMatchers.isDisplayed()));
    }

    @After
    public void afterTest() {
        mActivityRule.getActivity().finish();
    }

    private void logout() {
        UserUtils.getInstance().setLogoff(mActivityRule.getActivity());
        mActivityRule.getActivity().startActivity(CallManager.welcome(mActivityRule.getActivity()));
        mActivityRule.getActivity().finish();
    }
}
