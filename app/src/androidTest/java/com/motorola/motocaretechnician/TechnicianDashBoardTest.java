package com.motorola.motocaretechnician;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.motorola.motocaretechnician.view.ActivityMain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TechnicianDashBoardTest {
    CountDownLatch countDownLatch = new CountDownLatch(10);

    @Rule
    public ActivityTestRule<ActivityMain> mActivityRule = new ActivityTestRule<>(ActivityMain.class);

    @Test
    public void dashboard_shouldBeRefreshble() throws InterruptedException {
        chatProcedure();
        acceptedRequestProdure();
        pendingRequestProdure();
        openServicesProdure();

        countDownLatch.await(5, TimeUnit.SECONDS);

        onView(withId(R.id.swipeRefreshLayout)).perform(swipeDown());
    }

    @Test
    public void chatbutton_shouldOpenChatLobby() throws InterruptedException {
        onView(withId(R.id.rlChat)).perform(click());
        countDownLatch.await(15, TimeUnit.SECONDS);
        onView(withId(R.id.rvChatLobby)).check(matches(isDisplayed()));
        pressBack();
    }

    @Test
    public void acceptedRequestButton_shouldOpenAcceptedRequestList() {
        onView(withId(R.id.rlAcceptedRequests)).perform(click());
        pressBack();
    }

    @Test
    public void pendingRequestButton_shouldOpenPedingRequestList() {
        onView(withId(R.id.rlPendingRequests)).perform(click());
        pressBack();
    }

    @Test
    public void openServicesButton_shouldOpenOpenServiceList() {
        onView(withId(R.id.rlOpenedService)).perform(click());
        pressBack();
    }

    private void chatProcedure() {
        onView(withId(R.id.tvDescriptionChat)).check(matches(isDisplayed()));

    }

    private void acceptedRequestProdure() {
        onView(withId(R.id.tvDescriptionAccepted)).check(matches(isDisplayed()));
    }

    private void pendingRequestProdure() {
        onView(withId(R.id.tvDescriptionPending)).check(matches(isDisplayed()));
    }

    private void openServicesProdure() {
        onView(withId(R.id.tvDescriptionServices)).check(matches(isDisplayed()));
    }

}
